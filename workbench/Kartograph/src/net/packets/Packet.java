package net.packets;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 * Basic interface for packets.
 * Packets do have an ID to indicate their type.
 * Packet IDs are registered hardcoded in the PacketIDRegistry file.
 * @author Loris
 *
 */
public interface Packet {

	/**
	 * Returns the ID of this packet.
	 * The ID is a byte (0-255) which is prefixed the actual data.
	 * 
	 * @return The ID of this packet.
	 */
	public int getId();
	
	/**
	 * Writes to the given DataOutputStream.
	 * Do not forget to flush the stream!
	 * @param to The DataOutputStream to write to.
	 * @throws IOException If an exception occurs
	 */
	public void write(DataOutputStream to) throws IOException;
	
	/**
	 * Reads from the given DataInputStream.
	 * Classes which implement that interface are responsible to
	 * read all the data that belongs to them.
	 * @param from The DataOutputStream to read from.
	 * @throws IOException If an exception occurs
	 */
	public void read(DataInputStream from) throws IOException;
}
