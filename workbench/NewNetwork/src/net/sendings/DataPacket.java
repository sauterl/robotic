package net.sendings;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 * An interface to send anything over DataInput- and DataOutputstreams.
 * How and what to send is part of the implementing class.
 * @author Loris
 *
 */
public interface DataPacket<E> {
	
	/**
	 * Returns the ID of this packet.
	 * The ID is a byte (0-255) which is prefixed the actual data.
	 * 
	 * @return The ID of this packet.
	 */
	public int getId();
	
	public E get();
	
	public void set(E e);
	
	/**
	 * Writes to the given DataOutputStream.
	 * Do not forget to flush the stream!
	 * @param to The DataOutputStream to write to.
	 * @throws IOException If an exception occurs
	 */
	public void write(DataOutputStream to) throws IOException;
	
	/**
	 * Reads from the given DataInputStream.
	 * Classes which implement that interface are responsible to
	 * read all the data that belongs to them.
	 * @param from The DataOutputStream to read from.
	 * @throws IOException If an exception occurs
	 */
	public void read(DataInputStream from) throws IOException;
}
