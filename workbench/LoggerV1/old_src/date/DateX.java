/**
 * 
 */
package date;

/**
 * A wrapper and helper class to represent a date. This class was written
 * because of leJOS supports only a very unpowerful and poor Date.
 * 
 * @author loris.sauter
 * @version 0.1
 */
public class DateX {

	private long unixT;
	private int dayOfYear;
	private int year;
	private int month;
	private int day;
	private int hour;
	private int minutes;
	private int seconds;
}
