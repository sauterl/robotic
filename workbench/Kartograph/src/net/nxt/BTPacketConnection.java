package net.nxt;

import java.io.IOException;

import lejos.nxt.comm.Bluetooth;
import lejos.nxt.comm.NXTConnection;
import logging.ExceptionFileLogger;
import net.packets.AbstractPacketHandler;
import net.packets.EndOfStreamException;
import net.packets.NotSetupException;
import net.packets.PacketException;

/**
 * Class to handle the bluetooth connection with the DataPacket protocol.
 * @author Loris
 *
 */
public class BTPacketConnection {
	
	private final NXTConnection connection;
	
	private final AbstractPacketHandler packetHandler;
	private volatile boolean active = true;
	
	public BTPacketConnection(AbstractPacketHandler packetHandler){
		connection = Bluetooth.waitForConnection();
		this.packetHandler = packetHandler;
		if(connection != null){
			this.packetHandler.setup(connection.openDataInputStream(), connection.openDataOutputStream());
		}
	}
	
	public void close(){
		System.out.println("Closing connection");
		packetHandler.close();
		this.connection.close();
	}
	
	public void communicate(){
		while(active){
			System.out.println("Comm...");
			boolean more = false;
			try {
				System.out.println("Read...");
				more = packetHandler.readNextPacket();
			} catch (IOException | EndOfStreamException e) {
				System.out.println("IO or EoS ex");
				ExceptionFileLogger.LOGGER.writeException(e);
				active = false;
			} catch (PacketException e){
				System.out.println("Pckt ex");
				ExceptionFileLogger.LOGGER.writeException(e);
				active = false;
			} catch (NotSetupException e) {
				System.out.println("PH ex");
				ExceptionFileLogger.LOGGER.writeException(e);
				this.packetHandler.setup(connection.openDataInputStream(), connection.openDataOutputStream());
			}
			if(!more){
				System.err.println("No more");
				active = false;
			}
		}
		close();
	}

}
