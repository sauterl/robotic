/**
 * 
 */
package crashlogger;

/**
 * This class represents the configuration of a crash logger.
 * It contains settings to the mode of the logger and others.
 * 
 * @author loris.sauter
 * @version 0.1
 */
public class Configuration {

	/**
	 * A flag to indicate that the {@link CrashLogger} logs onto the display only.
	 */
	public final static int DISPLAY_ONLY = 100;
	/**
	 * A flag to indicate that the {@link CrashLogger} logs onto the display and into a file.
	 */
	public final static int DISPLAY_AND_FILE = 0;
	/**
	 * A flag to indicate that the {@link CrashLogger} logs into a file only.
	 */
	public final static int FILE_ONLY = 200;
	
	/**
	 * The prefix of the file, will be followed by the date.
	 */
	private String filePrefix;
	private String fileSuffix;
	/**
	 * The mode.
	 */
	private int mode;
	
	
	private static final char FSEPARATOR = '-';
	
	public Configuration(){
		mode = 0;//DEFAULT
		filePrefix = "crashlog";
		fileSuffix = ".txt";
	}
	
	
}
