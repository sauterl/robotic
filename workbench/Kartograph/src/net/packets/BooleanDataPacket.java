package net.packets;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class BooleanDataPacket implements DataPacket<Boolean>{

	private boolean b;
	
	public BooleanDataPacket(){}
	
	public BooleanDataPacket(boolean b){
		this.b = b;
	}
	
	@Override
	public int getId() {
		return PacketIDRegistry.BOOLEAN;
	}

	@Override
	public Boolean get() {
		return b;
	}

	@Override
	public void set(Boolean e) {
		this.b = e;
	}

	@Override
	public void write(DataOutputStream to) throws IOException {
		to.write(this.getId());
		to.writeBoolean(b);
		to.flush();
	}

	@Override
	public void read(DataInputStream from) throws IOException {
		this.b = from.readBoolean();
	}

}
