package net.sendings;

/**
 * Class to add DataPacket IDs.
 * 
 * The mapping of IDs and DataPackets is hard coded!
 * 
 * @author Loris
 *
 */
public final class DataIDManager {

	
	public static final int BOOLEAN = 1;
	public static final int BYTE = 2;
	public static final int SHORT = 3;
	public static final int INTEGER = 4;
	public static final int FLOAT = 5;
	public static final int DOUBLE = 6;
	public static final int CHAR = 7;
	public static final int STRING = 8;
	
	
}
