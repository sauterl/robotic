package vectormap;

public class OversizeException extends RuntimeException {
	/**
	 * This Exception is thrown if a datatype runs out of space.
	 */
	public OversizeException(String msg) {
		super(msg);
	}
}
