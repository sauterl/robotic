package net.nxt;

import lejos.geom.Point;
import logic.NXTControler;
import net.nxt.commands.CloseSequenz;
import net.nxt.commands.CommandException;
import net.nxt.commands.CommandSequenz;
import net.nxt.commands.ConnectSequenz;
import net.nxt.commands.IllegalSequenz;
import net.nxt.commands.InitSequenz;
import net.nxt.commands.MesureSequenz;
import net.nxt.commands.NextPointSequenz;
import net.nxt.commands.OKSequenz;
import net.nxt.commands.SendMapSequenz;
import net.nxt.commands.StartSequenz;
import net.packets.BasePacketHandler;
import net.packets.PacketIDRegistry;
import net.packets.commands.CommandPacket;

public class NXTCmdInterpreter implements CommandInterpreter {
	
	private NXTControler controller = null;
	
	public void createController(int size, Point start){
		if(controller == null){
			controller = new NXTControler(size, start);
		}
		//not ok
	}
	
	public NXTControler getController(){
		return controller;
	}
	
	public static int getCmdID(int packetId) {
		return packetId - PacketIDRegistry.CMD;
	}

	public static CommandSequenz getSequenzForPacketId(int id) throws IllegalArgumentException{
		switch (id) {
		case PacketIDRegistry.CMD_CONNECT:
			return new ConnectSequenz();
		case PacketIDRegistry.CMD_CLOSE:
			return new CloseSequenz();
		case PacketIDRegistry.CMD_OK:
			return new OKSequenz();
		case PacketIDRegistry.CMD_START:
			return new StartSequenz();
		case PacketIDRegistry.CMD_NEXTPOINT:
			return new NextPointSequenz();
		case PacketIDRegistry.CMD_MESURE:
			return new MesureSequenz();
		case PacketIDRegistry.CMD_MAP:
			return new SendMapSequenz();
		case PacketIDRegistry.CMD_INIT:
			return new InitSequenz();
		case PacketIDRegistry.CMD:
		case PacketIDRegistry.CMD_LAST:
			return new IllegalSequenz();
		default:
			throw new IllegalArgumentException("ERR: no seq: "+id);
		}
	}

	private BasePacketHandler ph;

	public NXTCmdInterpreter(BasePacketHandler ph) {
		this.ph = ph;
	}

	@Override
	public boolean interpretCommand(CommandPacket cmdPckt)
			throws CommandException {

		int cmdID = cmdPckt.getId() - PacketIDRegistry.CMD;
		System.out.println("CMD: " + cmdID);
		
		CommandSequenz seq;
		try{
			seq = getSequenzForPacketId(cmdPckt.getId());
			if(seq == null){
				throw new IllegalArgumentException("ERR: illegal seq");
			}
		}catch(IllegalArgumentException ex){
			throw new CommandException("ERR: illegal: " + cmdID);
		}
		seq.setInterpreter(this);
		seq.setPacketHandler(ph);
		boolean ret = seq.executeSequenz(cmdPckt);
		seq.removePacketHandler();
		seq.removeController();
		
		return ret;

		// OLD CODE:
		// switch (cmdPckt.getId()) {
		// case PacketIDRegistry.CMD_TEST:
		// // DO THE ACTUAL TEST
		// try {
		// ph.send(new OKCmdPacket());
		// System.out.println("Sent: OK");
		// ph.send(new StringDataPacket("Hello world"));
		// System.out.println("Sent: msg");
		// return true;
		// } catch (NotSetupException e1) {
		// System.out.println("ERR: ph setup");
		// e1.printStackTrace();
		// return false;
		// } catch (IOException e1) {
		// System.out.println("ERR: IO");
		// e1.printStackTrace();
		// return false;
		// }
		// case PacketIDRegistry.CMD_OK:
		// return true;
		//
	}

}
