package logging;

/**
 * A logger that is empty - thus logs nothing.
 * @author loris.sauter
 * @version 0.1
 */
public class EmptyLogger implements Logger{
	
	private byte level;
	private String name;
	public EmptyLogger(String name, byte level){
		this.level = level;
		this.name = name;
	}
	@Override
	public String getName() {
		return name;
	}
	@Override
	public void log(String msg) {
	}
	@Override
	public void debug(String msg) {
	}
	@Override
	public void info(String msg) {
	}
	@Override
	public void warn(String msg) {
	}
	@Override
	public void error(String msg) {
	}
	@Override
	public byte getLevel() {
		return level;
	}
	
}
