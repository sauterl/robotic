/**
 * 
 */
package net.packets;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import vectormap.Line;

/**
 * @author loris.sauter
 *
 */
public class VectorMapDataPacket implements DataPacket<Line[]> {
	
	private Line[] lines;
	
	/**
	 * 
	 */
	public VectorMapDataPacket() {}
	
	public VectorMapDataPacket(Line[] lines){
		this.lines = lines;
	}

	/** 
	 * {@inheritDoc}
	 * @see net.packets.Packet#getId()
	 */
	@Override
	public int getId() {
		return PacketIDRegistry.VECTOR_MAP;
	}

	/** 
	 * {@inheritDoc}
	 * @see net.packets.Packet#write(java.io.DataOutputStream)
	 */
	@Override
	public void write(DataOutputStream to) throws IOException {
		to.write(this.getId() );
		to.writeInt(lines.length);
		for(Line l : lines){
			to.writeFloat(l.getStartX());
			to.writeFloat(l.getStartY());
			to.writeFloat(l.getEndX());
			to.writeFloat(l.getEndY());
		}
		to.flush();
	}

	/** 
	 * {@inheritDoc}
	 * @see net.packets.Packet#read(java.io.DataInputStream)
	 */
	@Override
	public void read(DataInputStream from) throws IOException {
		int size = from.readInt();
		lines = new Line[size];
		for(int i=0; i<size; i++){
			float ex,ey,sx,sy;
			sx = from.readFloat();
			sy = from.readFloat();
			ex = from.readFloat();
			ey = from.readFloat();
			lines[i] = new Line(sx, sy, ex, ey);
		}
	}

	@Override
	public Line[] get() {
		return lines;
	}

	@Override
	public void set(Line[] e) {
		this.lines = e;
	}

}
