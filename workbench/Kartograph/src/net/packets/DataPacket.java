package net.packets;


/**
 * An interface to send anything over DataInput- and DataOutputstreams.
 * How and what to send is part of the implementing class.
 * @author Loris
 *
 */
public interface DataPacket<E> extends Packet{
	
	/**
	 * Returns whatever needed.
	 * This is kind of experimental and may get removed in a future update
	 * @return whatever needed.
	 */
	public E get();
	
	/**
	 * Sets whatever needed.
	 * This is kind of experimental and may get removed in a future update
	 * @param e The Element to set.
	 */
	public void set(E e);
	
}
