package junit;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import junit.testVectormapTinyList;

@RunWith(Suite.class)
@SuiteClasses({testVectormapTinyList.class})
public class testVectormapPackage {

}
