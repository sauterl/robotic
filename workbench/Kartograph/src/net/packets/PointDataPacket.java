/**
 * 
 */
package net.packets;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import lejos.geom.Point;

/**
 * A DataPacket to send Point (The lejso.geom ones).
 * @author loris.sauter
 *
 */
public class PointDataPacket implements DataPacket<Point> {
	
	private Point p = null;
	
	public PointDataPacket(){}
	
	public PointDataPacket(Point p){
		this.p = p;
	}

	@Override
	public int getId() {
		return PacketIDRegistry.POINT;
	}

	@Override
	public void write(DataOutputStream to) throws IOException {
		to.write(this.getId());
		to.writeFloat(p.x);
		to.writeFloat(p.y);
		to.flush();
	}

	@Override
	public void read(DataInputStream from) throws IOException {
		float x,y;
		x = from.readFloat();
		y = from.readFloat();
		this.p = new Point(x,y);
	}

	@Override
	public Point get() {
		return p;
	}

	@Override
	public void set(Point e) {
		this.p = e;
	}

}
