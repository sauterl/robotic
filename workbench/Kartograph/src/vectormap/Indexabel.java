package vectormap;

public interface Indexabel<E> {
	/**
	 * Return a Index for a specific type of Object. used to make calsses exchangabel.
	 * 
	 * @param element element to index
	 * @return the index of the element
	 */
	public short getindex(E element);

	/**
	 * Get the number of indexes the indexer generates,
	 * 
	 * @return number of indexes generated
	 */
	public short getNumIndex();
}
