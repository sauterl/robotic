package net.pc.commands;

import net.packets.Packet;
import net.packets.commands.ConnectCmdPacket;
import net.packets.commands.OKCmdPacket;

public class ConnectCommand extends AbstractCommand {

	public ConnectCommand(){
		super(new ConnectCmdPacket() );
	}

	@Override
	public boolean onReceive(Packet answer) {
		if(answer instanceof OKCmdPacket){
			System.out.println("All right, got OK after CONNECT");
			return true;
		}else{
			System.out.println("Got packet if ID "+answer.getId()+" after CONNECT.");
			return false;
		}
		
	}

}
