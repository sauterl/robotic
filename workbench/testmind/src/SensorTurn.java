import lejos.nxt.Button;
import lejos.nxt.LCD;
import lejos.nxt.Motor;
import lejos.nxt.SensorPort;
import lejos.nxt.UltrasonicSensor;
import lejos.nxt.addon.CompassHTSensor;


public class SensorTurn {
public static void main(String []args){
	int c=0;
	CompassHTSensor sens=new CompassHTSensor(SensorPort.S2);
	Motor.B.setSpeed(36);
	Motor.C.setSpeed(36);
	if(true){
	sens.startCalibration();
	Motor.B.rotate(1000*2,true);
	Motor.C.rotate(-1000*2);
	sens.stopCalibration();
	sens.resetCartesianZero();
	}
	Motor.B.rotate(1000*2,true);
	Motor.C.rotate(-1000*2,true);
	boolean b=true;
	while(c<1){
		System.out.println(sens.getDegrees());
		if(b){
			Motor.A.rotate(10,true);
		}else{
			Motor.A.rotate(-10,true);
		}
		b=!b;
	}
	
	
	
	int count=0;
//	Motor.A.setSpeed(10000);
	Motor.B.setAcceleration(360);
	Motor.C.setAcceleration(360);
//	while(count<10){
	int turn=735;
	while(true){
//		if(Button.ENTER.isDown()){
//			Motor.B.rotate(turn*5,true);
//			Motor.C.rotate(-turn*5);
//			count+=1;
//			System.out.println(count);
//		}
		
		
		UltrasonicSensor son=new UltrasonicSensor(SensorPort.S1);
		float [] mes=mesreg(son.getRanges(),son.getRanges(),son.getRanges(),son.getRanges(),son.getRanges());
		LCD.clear();
		for (float f:mes){
			System.out.println(f);
		}
		while(Button.ENTER.isUp()){
			
		}
		
	}
}


public static float[] mesreg(float[]... mesurs){
	int [] counter=new int[8];
	for(float[]f:mesurs){
		counter[f.length]++;
	}
	int bestL=0;
	for(int i=1;i<8;i++){
		if(counter[bestL]<counter[i]){
			bestL=i;
		}
	}
	float [] avg =new float[bestL];
	int c=0;
	for(float[]f:mesurs){
		if(f.length==bestL){
			c++;
			for(int i=0;i<bestL;i++){
				avg[i]+=f[i];
			}
		}
	}
	for(int i=0;i<bestL;i++){
		avg[i]=avg[i]/c;
	}
	return avg;
}

private static float getAvrage(float[] ar){
	double d=0;
	int c=0;
	for (float f:ar){
		if (f!=0){
		d+=f;
		c++;
		}
	}
	return (float)d/c;
	}
}