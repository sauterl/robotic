package logging;

/**
 * The base class for all (default) {@link Logger}s. This class simply has a
 * name and a log level. The child class is responsible for the actual logging
 * mechanic and log level handling.
 * 
 * @author loris.sauter
 * @version 0.1
 * 
 */
public abstract class AbstractLogger implements Logger {

	private final byte level;
	private final String name;

	/**
	 * Creates a new {@link AbstractLogger} with the given name and the given
	 * level.
	 * 
	 * @param name
	 *            The name of this logger
	 * @param level
	 *            The level on which this logger logs
	 */
	protected AbstractLogger(String name, byte level) {
		this.level = level;
		this.name = name;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getName() {
		return name;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public abstract void log(String msg);

	/**
	 * {@inheritDoc}
	 */
	@Override
	public abstract void debug(String msg);

	/**
	 * {@inheritDoc}
	 */
	@Override
	public abstract void info(String msg);

	/**
	 * {@inheritDoc}
	 */
	@Override
	public abstract void warn(String msg);

	/**
	 * {@inheritDoc}
	 */
	@Override
	public abstract void error(String msg);

	/**
	 * {@inheritDoc}
	 */
	@Override
	public byte getLevel() {
		return level;
	}

}
