package logic;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileWriter {
	private DataOutputStream dataOut;
	private FileOutputStream out;

	public FileWriter(String Filename) throws FileNotFoundException {
		out = null;
		File data = new File(Filename);
		out = new FileOutputStream(data);
		dataOut = new DataOutputStream(out);
	}

	public void close() throws IOException {
		// if(out!=null){
		out.close();
		// }
	}

	// public void write(Object obj) throws IOException {
	// write(obj.toString());
	// }

	public void write(String text) throws IOException {
		dataOut.writeBytes(text);
	}
}
