package net.test;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import lejos.pc.comm.NXTComm;
import lejos.pc.comm.NXTCommException;
import lejos.pc.comm.NXTCommFactory;
import lejos.pc.comm.NXTInfo;
import net.packets.PacketIDRegistry;
import net.packets.StringDataPacket;
import vectormap.Line;

/**
 * PC INITIATOR
 * 
 * Establishes a bluetooth connection to all available nxt bricks and then sends them
 * some things to test.
 * @author Loris
 *
 */
public class PCTest {
	public static void main(String[] args){
		NXTComm com = null;
		try {
			com = NXTCommFactory.createNXTComm(NXTCommFactory.BLUETOOTH);
		} catch (NXTCommException e) {
			e.printStackTrace();
			die();
		}
//		NXTInfo info = new NXTInfo(NXTCommFactory.BLUETOOTH, "NXT", "");
		NXTInfo[] infos = null;
		try {
			infos = com.search("NXT");
		} catch (NXTCommException e) {
			e.printStackTrace();
			die();
		}
		if(infos != null){
			for(NXTInfo nfo : infos){
				try {
					com.open(nfo);
					System.out.println("Established connection");
					InputStream is = com.getInputStream();
					OutputStream os = com.getOutputStream();
					DataInputStream dis = new DataInputStream(is);
					DataOutputStream dos = new DataOutputStream(os);
					/* OLD TEST*/
//					for(int i=0;i<100;i++){
//						dos.writeInt(i);
//						dos.flush();
//						int read = dis.readInt();
//						System.out.println("Read: "+read);
//					}
					/*PacketHandler and Line implements DataPacket*/
					Line l = new Line(1,1,4,4);
					l.write(dos);
					Line l1 = new Line(2,2,3,3);
					l1.write(dos);
					
					/* StringDataPacket test */
//					StringDataPacket strp = new StringDataPacket("Hello");
//					strp.write(dos);
					int flag = dis.read();
					if(flag == PacketIDRegistry.STRING){
						StringDataPacket in = new StringDataPacket();
						in.read(dis);
						System.out.println("Read: "+in.get());
					}else{
						System.out.println("Flag: "+flag+" did not expect that!");
					}
				} catch (NXTCommException | IOException e) {
					e.printStackTrace();
				}
				
			}
		}
	}
	
	
	public static void die(){
		System.err.println("Dying now!");
		System.exit(-1);
	}
	
	public static void die(String msg){
		System.err.println(msg);
		System.err.println("Dying now!");
		System.exit(-1);
	}
}
