package net.packets;

/**
 * Class to represent exceptions caused by an end of stream.
 * @author Loris
 * @version 1.0
 */
public class EndOfStreamException extends Exception {
	
	/**
	 * Creates a new EndOfStreamException with the default message: "End of Stream got reached".
	 */
	public EndOfStreamException(){
		super("End of Stream got reached.");
	}
	
	/**
	 * Creates a new EndOfStreamException with a custom message.
	 * @param msg The message of this exception
	 */
	public EndOfStreamException(String msg){
		super(msg);
	}
	
	/**
	 * Creates a new EndOfStreamException for a given cause.
	 * @param cause The cause of this exception
	 */
	public EndOfStreamException(Throwable cause){
		super(cause);
	}
	
	/**
	 * Creates a new EndOfStreamException for a gievn cause and with a custom message.
	 * @param msg
	 * @param cause
	 */
	public EndOfStreamException(String msg, Throwable cause){
		super(msg, cause);
	}

}
