package net.nxt.commands;

import logic.NXTControler;
import net.nxt.NXTCmdInterpreter;
import net.packets.BasePacketHandler;
import net.packets.Packet;

/**
 * Interface to provide executable code on the nxt
 * @author loris.sauter
 *
 */
public abstract class CommandSequenz {
	
	protected BasePacketHandler handler = null;
	protected NXTCmdInterpreter interpreter = null;
	
	public void setInterpreter(NXTCmdInterpreter interpreter){
		this.interpreter = interpreter;
	}
	
	public void removeController(){
		this.interpreter = null;
	}
	
	public void setPacketHandler(BasePacketHandler ph){
		this.handler = ph;
	}
	
	public void removePacketHandler(){
		this.handler = null;
	}
	
	/**
	 * Gets called when the command interpreter read the command packet.
	 * @return
	 */
	public abstract boolean executeSequenz(Packet packet) throws SequenzException;

}
