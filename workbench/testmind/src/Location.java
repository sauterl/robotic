
public class Location {
	private double x,y;
	private int direction;
public Location(double x, double y,int direction){
	this.x=x;
	this.y=y;
	this.direction=direction;
}
public double getX(){
	return x;
}
public double getY(){
	return y;
}
public int getDirection(){
	return direction;
	
}
public void setX(double x){
	this.x=x;
}
public void setY(double y){
	this.y=y;
}
public void setDirection(int direction){
	this.direction=direction;
}
public void turn(int angle){
	direction+=angle;
}
public void moveX(double distance){
	x+=distance;
}
public void moveY(double distance){
	y+=distance;
}
public void move(double distance){
	x+=distance*Math.cos(180/direction*Math.PI);
	y+=distance*Math.sin(180/direction*Math.PI);
}
}
