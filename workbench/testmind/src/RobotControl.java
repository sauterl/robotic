import lejos.nxt.Motor;
import lejos.nxt.SensorPort;
import lejos.nxt.UltrasonicSensor;


public class RobotControl {
	private static final double BRAKEDEGREES = 10;
	/**
	 * Wheel diameter
	 */
	private static final double DIAMETER=5.6;
	private UltrasonicSensor son;
	private Mesure m;
	private Location loc;
	/**
	 * default constructor
	 */
	public RobotControl(){
		son=new UltrasonicSensor(SensorPort.S1);
		m=new Mesure(son);
		loc=new Location(0, 0, 0);
	}
	public RobotControl(Mesure meth){
		son=new UltrasonicSensor(SensorPort.S1);
		m=meth;
		meth.setSensor(son);
		loc=new Location(0, 0, 0);
	}
	
	
	/**
	 * drives the robot forward in a straight line.
	 * @param distance
	 */
	public void drive(int distance){
		double degrees =360*distance/(DIAMETER*Math.PI);
		Motor.B.resetTachoCount();
		Motor.B.rotate((int)degrees,true);
		Motor.C.rotate((int)degrees,false);
		loc.move(distance);
	}
	
	public void drive(int distance, boolean ImieiateReturn){
		double degrees =360*distance/(DIAMETER*Math.PI);
		Motor.B.rotate((int)degrees,true);
		Motor.C.rotate((int)degrees,ImieiateReturn);
		loc.move(distance);
	}
	
	public void turn(int angle){
		Setting.turnSettings();
		angle=(int)(720.0*(double)angle/360.0);
		Motor.B.rotate(-angle,true);
		Motor.A.rotate(angle);
		loc.turn(angle);
	}
	
	public void turn(int angle, boolean ImieiateReturn){
		Setting.turnSettings();
		angle=(int)(720.0*(double)angle/360.0);
		Motor.B.rotate(-angle,true);
		Motor.A.rotate(angle,ImieiateReturn);
		loc.turn(angle);
	}
	
	public void takeMesure(){
		m.takeMesure(loc.getX(), loc.getY(), loc.getDirection()); 
	}

	
	
	
}
