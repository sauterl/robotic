package logic;

import lejos.geom.Point;
import lejos.robotics.navigation.Waypoint;
import mesure.Mesure;
import control.RobotAboutToCrashException;
import control.RobotControl;

public class Tdr {

	public static void main(String[] args) {
		Mesure mes = null;
		RobotControl contr = new RobotControl(mes);

		contr.turnToDegree(0);
		// contr.drive(10);
		// System.out.println(contr.getLocation().getPosition());
		// contr.turnToDegree(90);
		// contr.drive(10);
		// System.out.println(contr.getLocation().getPosition());
		// contr.turnToDegree(180);
		// contr.drive(10);
		// System.out.println(contr.getLocation().getPosition());
		// contr.turnToDegree(270);
		// contr.drive(10);
		// System.out.println(contr.getLocation().getPosition());

		try {
			contr.drive(new Waypoint(new Point(25, 0)));
			System.out.println(contr.getLocation().getPosition());
			contr.drive(new Waypoint(new Point(25, 25)));
			System.out.println(contr.getLocation().getPosition());
			contr.drive(new Waypoint(new Point(0, 25)));
			System.out.println(contr.getLocation().getPosition());
			contr.drive(new Waypoint(new Point(-25, 25)));
			System.out.println(contr.getLocation().getPosition());
			contr.drive(new Waypoint(new Point(-25, 0)));
			System.out.println(contr.getLocation().getPosition());
			contr.drive(new Waypoint(new Point(-25, -25)));
			System.out.println(contr.getLocation().getPosition());
			contr.drive(new Waypoint(new Point(0, -25)));
			System.out.println(contr.getLocation().getPosition());
			contr.drive(new Waypoint(new Point(25, -25)));
			System.out.println(contr.getLocation().getPosition());

			contr.drive(new Waypoint(new Point(-25, 25)));
			System.out.println(contr.getLocation().getPosition());

			contr.drive(new Waypoint(new Point(25, 25)));
			System.out.println(contr.getLocation().getPosition());
			contr.drive(new Waypoint(new Point(-25, -25)));
			System.out.println(contr.getLocation().getPosition());

			contr.drive(new Waypoint(new Point(0, 0)));
			System.out.println(contr.getLocation().getPosition());

		} catch (RobotAboutToCrashException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
