package net.test;

import lejos.geom.Point;
import lejos.nxt.Button;
import logging.ExceptionFileLogger;
import net.nxt.BTPacketConnection;
import net.nxt.NXTPacketHandler;

public class TestRobot {
	
	public static void main(String[] args){
		System.out.println("started...");
		BTPacketConnection con = new BTPacketConnection(new NXTPacketHandler() );
		System.out.println("Starting communicate");
		con.communicate();
		System.out.println("Finished communicate");
		ExceptionFileLogger.LOGGER.close();
		Button.waitForAnyPress();
	}

}
