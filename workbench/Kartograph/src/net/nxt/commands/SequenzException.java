/**
 * 
 */
package net.nxt.commands;

/**
 * Base exception while executing sequenz
 * @author loris.sauter
 *
 */
public class SequenzException extends CommandException {

	/**
	 * 
	 */
	public SequenzException() {
		super("ERR: Seq.exec");
	}

	/**
	 * @param message
	 */
	public SequenzException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

}
