package net.pc.commands;

import java.io.IOException;

import net.packets.EndOfStreamException;
import net.packets.NotSetupException;
import net.packets.Packet;
import net.packets.PacketException;
import net.packets.StringDataPacket;
import net.packets.commands.ReqTestCmdPacket;

public class RequestPacketTestCommand extends AbstractCommand {

	public RequestPacketTestCommand() {
		super(new ReqTestCmdPacket() );
	}

	@Override
	public boolean onReceive(Packet answer) throws UnexpectedAnswerException {
		Packet p = null;
		try {
			System.out.println("Okay, expecting next packet");
			 p = this.handler.getNextPacket();
			 this.handler.readPacket(p);
			 if(p instanceof StringDataPacket){
				 String ans = ((StringDataPacket)p).get();
				 System.out.println("Next packet's content: "+ans);
			 }else{
				 throw new UnexpectedAnswerException(p);
			 }
			 return true;
		} catch (PacketException e) {
			e.printStackTrace();
			throw new UnexpectedAnswerException(p);
		} catch (IOException | NotSetupException e) {
			System.out.println("IOE or NSE");
			e.printStackTrace();
			return false;
		} catch (EndOfStreamException e) {
			System.out.println("End of stream reached");
			e.printStackTrace();
			return false;
		}
	}

}
