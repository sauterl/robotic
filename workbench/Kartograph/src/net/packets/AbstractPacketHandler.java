package net.packets;

import java.io.IOException;

/**
 * The AbstractPacketHandler handles incoming and outgoing packets.
 * 
 * The abstract class can only read and send packets to another packet handler,
 * connected to corresponding DataInputStream and DataOutputstreams.
 * 
 * To get an actual working PacketHandler one must implement the method onPacketReceived(DataPacket)
 * 
 * @author Loris
 * @version 0.1
 */
public abstract class AbstractPacketHandler extends BasePacketHandler{

	
	
	/**
	 * Reads the next packet on the stream and calls onPacketReceived with that read packet.
	 * @return TRUE on success, FALSE if no more packets are expected
	 * @throws PacketException If the received packet ID is not supported
	 * @throws IOException If an IO error occurs
	 * @throws EndOfStreamException If the end of stream got reached while reading the packet
	 * @throws NotSetupException If the Handler is not setup correctly (setup must be called properly)
	 */
	public boolean readNextPacket() throws PacketException, IOException, EndOfStreamException, NotSetupException{
		Packet packet = getNextPacket();
		if(packet != null){
			packet.read(dis);
			return onPacketReceived(packet);
		}
		throw new EndOfStreamException("While reading the packet, the End Of Stream got reached.");
	}
	
	/**
	 * Gets called whenever a packet got successfully read.
	 * Implement this method to do something when a packet got received.
	 * @param dp The packet that got received.
	 * @return TRUE on success, FALSE if no more packets are expected
	 */
	public abstract boolean onPacketReceived(Packet packet);
	
}
