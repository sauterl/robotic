package logging;

/**
 * The LogConfig describes how a {@link Logger} has to log. On which level the logging happens and further information.
 * @author loris.sauter
 * @version 0.1
 *
 */
public class LogConfig {

	public static final byte DEBUG_LEVEL = 0;
	public static final byte INFO_LEVEL = 8;
	public static final byte WARN_LEVEL = 16;
	public static final byte ERROR_LEVEL = 32;
	
	/**
	 * Formats for a given {@link Logger} its message.
	 * This method is designed, so that the logger itself does not need to worry on how to display logs.
	 * @param logger The logger that wants its log formatted
	 * @param msg The log message
	 * @return A formatted, printable line to log.
	 */
	public static String formatEasy(Logger logger, String msg){
		String name = logger != null ? (logger.getName() != null || logger.getName().length() > 0 ? logger.getName() : "N/A") : "null";
		String lvl = logger != null ? getLevelName(logger.getLevel() ) : "null";
		return "["+name+"/"+lvl+"]"+msg;
	}
	
	/**
	 * Returns the name of a logging level. This name is readable by human.
	 * @param level The level to convert
	 * @return The human readable name of a logging level.
	 */
	public static String getLevelName(byte level){
		switch(level){
		case DEBUG_LEVEL:
			return "DEBUG";
		case INFO_LEVEL:
			return "INFO";
		case WARN_LEVEL:
			return "WARN";
		case ERROR_LEVEL:
			return "ERROR";
		default:
			return "UNKOWN";
		}
	}
	
	private byte level;
	/**
	 * Returns the level that this configuration requests
	 * @return The log level that this configuration requests
	 */
	public byte getLevel(){
		return level;
	}
	/**
	 * Sets the level for this configuration - only change this _before_ applying to the log manager!
	 * @param level The level for this configruation.
	 */
	public void setLevel(byte level){
		this.level = level;
	}
	
	/**
	 * Creates the default {@link LogConfig}.
	 * By default log level warn is set.
	 */
	public LogConfig(){
		level = WARN_LEVEL;
	}
	
	/**
	 * Creates a LogConfig for a given level.
	 * @param level the log level.
	 */
	public LogConfig(byte level){
		this.level = level;
	}
}
