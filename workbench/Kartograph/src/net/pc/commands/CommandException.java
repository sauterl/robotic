package net.pc.commands;

/**
 * An exception to indicate an error with commands.
 * @author Loris
 *
 */
public class CommandException extends RuntimeException {

	public CommandException(String msg) {
		super(msg);
	}

}
