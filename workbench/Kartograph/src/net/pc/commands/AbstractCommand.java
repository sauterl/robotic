package net.pc.commands;

import logic.PCControler;
import net.packets.BasePacketHandler;
import net.packets.Packet;
import net.packets.commands.CommandPacket;

/**
 * The AbstractCommand is the base class for all commands.
 * It provides a basic implementation and methos to hook in.
 * @author loris.sauter
 *
 */
public abstract class AbstractCommand {

	private CommandPacket cmdPacket;
	/**
	 * The packethandler which handles the packets on the channel, which this command is executed
	 */
	protected BasePacketHandler handler;
	
	protected PCControler controller;
	
	/**
	 * Creates an absctract command with the given underlying command packet.
	 * @param pckt the underlying command packet
	 */
	public AbstractCommand(CommandPacket pckt){
		this.cmdPacket = pckt;
	}
	
	/**
	 * Sets the Packethandler on which this command can send command internal packets
	 * @param bph The handler to set
	 */
	public void setPacketHandler(BasePacketHandler bph){
		this.handler = bph;
	}
	
	public void setController(PCControler ctrl){
		this.controller = ctrl;
	}

	/**
	 * Removes the internal Packethandler, so that this command cannot communicate.
	 */
	public void removePacketHandler(){
		this.handler = null;
	}
	
	public void removeController(){
		this.controller = null;
	}
	/**
	 * Returns the sendable representation of this command as a CommandPacket.
	 * @return the sendable representation of this command as a CommandPacket.
	 */
	public CommandPacket getCmdPacket() {
		return cmdPacket;
	}

	/**
	 * Sets the CommandPacket for this AbstractCommand
	 * @param cmdPacket The packet to send as sendable representation of this command
	 */
	public void setCmdPacket(CommandPacket cmdPacket) {
		this.cmdPacket = cmdPacket;
	}
	
	/**
	 * Gets called when the answer of this command got received
	 * During the VM is executing this method, the internal handler is not null and usable.
	 * @param answer The answer packet that got received
	 * @throws UnexpectedAnswerException If the answer packet is unexpected
	 */
	public abstract boolean onReceive(Packet answer) throws UnexpectedAnswerException;
	
	/**
	 * Gets called when the answer of this command was CMD_CLOSE.
	 * Commands that override this method MUST call super.onClose in the last
	 * line!
	 */
	public void onClose() throws ClosingConnectionException{
		throw new ClosingConnectionException();
	}

}
