/**
 * 
 */
package pc;

import java.util.concurrent.ArrayBlockingQueue;

/**
 * Class that is capable of executing an internal queue of tasks.
 * 
 * The internal queue is an ArrayBlockingQueue to block until a next task is available and to grant FIFO order.
 * @author loris.sauter
 *
 */
public class TaskExecutor implements Runnable {
	
	private ArrayBlockingQueue<Task> queue;
	private final static int queueCap = 30;
	
	private volatile boolean active = true;

	/**
	 * Creates a new TaskExecutor with an initial empty queue.
	 */
	public TaskExecutor() {
		queue = new ArrayBlockingQueue<Task>(queueCap);
	}
	
	/**
	 * Adds a Task to the internal queue.
	 * @param t The task to add
	 * @throws InterruptedException If interrupted while waiting.
	 * @see ArrayBlockingQueue#put(Object)
	 */
	public void putTask(Task t) throws InterruptedException{
		queue.put(t);
	}

	/** 
	 * Executes the task in the queue in FIFO order.
	 * {@inheritDoc}
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		while(active){
			Task currTask = null;
			try {
				currTask = queue.take();
			} catch (InterruptedException e) {
				System.err.println("Could not wait for next element");
				e.printStackTrace();
			}
			if(currTask != null){
				currTask.execute(this);
			}
		}
	}

}
