/**
 * 
 */
package net.packets.commands;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import net.packets.PacketIDRegistry;

/**
 * @author loris.sauter
 *
 */
public class CmdMesure implements CommandPacket {

	/** 
	 * {@inheritDoc}
	 * @see net.packets.Packet#getId()
	 */
	@Override
	public int getId() {
		return PacketIDRegistry.CMD_MESURE;
	}

	/** 
	 * {@inheritDoc}
	 * @see net.packets.Packet#write(java.io.DataOutputStream)
	 */
	@Override
	public void write(DataOutputStream to) throws IOException {
		to.write(this.getId());
		to.flush();
	}

	/** 
	 * {@inheritDoc}
	 * @see net.packets.Packet#read(java.io.DataInputStream)
	 */
	@Override
	public void read(DataInputStream from) throws IOException {

	}

	/** 
	 * {@inheritDoc}
	 * @see net.packets.commands.CommandPacket#executeCommand()
	 */
	@Override
	public void executeCommand() {

	}

}
