/**
 * 
 */
package net.packets;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import control.MesureMap;

/**
 * @author loris.sauter
 *
 */
public class MesureMapDataPacket implements DataPacket<MesureMap> {

	private MesureMap map;
	
	/**
	 * 
	 */
	public MesureMapDataPacket() {
	}
	
	public MesureMapDataPacket(MesureMap map){
		this.map = map;
	}

	/**
	 * For packets: Indicates that the current point is NOT messured AND NOT unreachabel
	 */
	public static final byte UNMES = 0;
	/**
	 * For packets: Indicates that the current point IS mesured AND NOT unreachabel => good
	 */
	public static final byte MES = 1;
	/**
	 * For packets: Indicates that the current point IS unreachabel
	 */
	public static final byte UNR = 2;
	/**
	 * For packets: Indicates that an error occurred and the current point neither mesured nor unreachabel or anything is
	 * (should not happen)
	 */
	private static final byte ERR = 7;
	
	
	@Override
	public int getId() {
		return PacketIDRegistry.MESURE_MAP;
	}
	
	@Override
	public void write(DataOutputStream to) throws IOException {
		to.write(this.getId() );
		to.writeInt(map.mesured.length);
		//always send cols per row
		for(int y=0; y<map.mesured.length; y++){
			for(int x=0; x<map.mesured.length; x++){
				if(map.mesured[x][y] && map.unreachabel[x][y]){
					to.writeByte(UNR);
				}else if(map.mesured[x][y] && !map.unreachabel[x][y]){
					to.writeByte(MES);
				}else if(!map.mesured[x][y] && map.unreachabel[x][y]){
					to.writeByte(UNR);
				}else if(!map.mesured[x][y] && !map.unreachabel[x][y]){
					to.writeByte(UNMES);
				}else{
					to.writeByte(ERR);
				}
			}
		}
		to.flush();
	}
	
	@Override
	public void read(DataInputStream from) throws IOException {
		int size = from.readInt();
		this.map = new MesureMap(size);
		map.mesured = new boolean[size][size];
		map.unreachabel = new boolean[size][size];
		for(int y = 0; y < size; y++){
			for(int x = 0; x < size; x++){
				int flag = from.readByte();
				switch(flag){
				case UNMES:
					map.mesured[x][y] = false;
					map.unreachabel[x][y] = false;
					break;
				case MES:
					map.mesured[x][y] = true;
					map.unreachabel[x][y] = false;
					break;
				case UNR:
					map.mesured[x][y] = true;
					map.unreachabel[x][y] = true;
					break;
				default:
					System.out.println("ERR: mesmap rec err");
					break;
				}
			}
		}
		
	}

	@Override
	public MesureMap get() {
		return map;
	}

	@Override
	public void set(MesureMap e) {
		this.map = e;
	}

}
