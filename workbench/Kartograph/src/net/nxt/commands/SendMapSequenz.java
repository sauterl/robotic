/**
 * 
 */
package net.nxt.commands;

import java.io.IOException;

import net.packets.NotSetupException;
import net.packets.Packet;
import net.packets.VectorMapDataPacket;

/**
 * @author loris.sauter
 *
 */
public class SendMapSequenz extends CommandSequenz {

	/** 
	 * {@inheritDoc}
	 * @see net.nxt.commands.CommandSequenz#executeSequenz(net.packets.Packet)
	 */
	@Override
	public boolean executeSequenz(Packet packet) throws SequenzException {
		try {
			handler.send(new VectorMapDataPacket(interpreter.getController().getVectormap().getLines()));
			return true;
		} catch (NotSetupException | IOException e) {
			e.printStackTrace();
		}
		return false;
	}

}
