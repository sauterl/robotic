import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import lejos.nxt.LCD;
import lejos.nxt.Motor;


public class DisplayWithLog extends Mesure {
	DataOutputStream writer;
	public DisplayWithLog(String filename){
		FileOutputStream out=null;
		File data=new File(filename);
		try{
		out=new FileOutputStream(data);
		}catch (FileNotFoundException e){
			System.out.println("problem");
		}
		writer=new DataOutputStream(out);
		try {
			writer.writeBytes("angle deg\tx\ty\r");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		clearMap();
	}
	
	public void takeMesure(double x, double y, int angle){
		for(int i=0;i<90;i++){
		float[] m=getdist();
		logMesure(x,y,angle+i*4,m);
//		displayMesure(angle+i*4,m);
		Motor.A.rotate(4);
		}
		Motor.A.rotate(-360);
	}
	
	

	private void displayMesure( double angle, float[] m) {
		if(m.length==0){
			return;
		}
		double x;
		double y;
		x=m[0]*Math.cos(angle/180*Math.PI);
		y=m[0]*Math.sin(angle/180*Math.PI);
//		System.out.println(Math.cos(angle/180*Math.PI)+"  "+Math.sin(angle/180*Math.PI));
		double sx=Math.sin(angle/180*Math.PI)*1;
		double sy=Math.cos(angle/180*Math.PI)*1;
//		while(Math.abs(x)<255&&Math.abs(y)<255){
			setPixel(y,x);
			x+=sx;
			y+=sy;
//			}
	}
	
	private void setPixel(double x,double y){
		x=x/255*-32;
		y=y/255*-50;
		LCD.setPixel(50+(int)y, 32+(int)x, 1);
		
	}

	private void logMesure(double x, double y, double angle, float[] m) {
		String a="";
		for (float f:m){
		a+=Double.toString(angle);
		a+="\t";
		a+=Double.toString(x+f*Math.cos(angle/180*Math.PI));
		a+="\t";
		a+=Double.toString(y+f*Math.sin(angle/180*Math.PI));
		a+="\r";
		try {
			writer.writeBytes(a);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
	}

	private float[] getdist() {
		return mesreg(sens.getRanges(),sens.getRanges(),sens.getRanges(),sens.getRanges(),sens.getRanges());
	}
	
	public float[] mesreg(float[]... mesurs){
		int [] counter=new int[8];
		for(float[]f:mesurs){
			counter[f.length]++;
		}
		int bestL=0;
		for(int i=1;i<8;i++){
			if(counter[bestL]<counter[i]){
				bestL=i;
			}
		}
		float [] avg =new float[bestL];
		int c=0;
		for(float[]f:mesurs){
			if(f.length==bestL){
				c++;
				for(int i=0;i<bestL;i++){
					avg[i]+=f[i];
				}
			}
		}
		for(int i=0;i<bestL;i++){
			avg[i]=avg[i]/c;
		}
		return avg;
	}
	
	public void clearMap(){
		LCD.clear();
		LCD.setPixel(50, 32, 1);
		LCD.setPixel(50, 31, 1);
		LCD.setPixel(49, 32, 1);
		LCD.setPixel(49, 31, 1);
	}
	
	public void close(){
		try {
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
