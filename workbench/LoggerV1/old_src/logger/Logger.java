/**
 * 
 */
package logger;

/**
 * Logger interface
 * @author Loris
 *
 */
public interface Logger {

	public void setLevel(LoggingLevel level);
	public LoggingLevel getLevel();
	
	public void log(String msg);
	public void log(char c);
	public void log(boolean b);
	public void log(float f);
	public void log(double d);
	public void log(byte b);
	public void log(short s);
	public void log(int i);
	public void log(long l);
	public void log(Object obj);
	public void log(Exception e);
}
