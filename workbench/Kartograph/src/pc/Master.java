package pc;

import logic.PCControler;
import net.pc.BTChannelManager;

/**
 * The master controls the two nxts
 * @author Loris
 *
 */
public class Master {
	
	public static void main(String[] args){
		PCControler master = new PCControler(500);
		BTChannelManager manager = new BTChannelManager(master);
		
		
	}

}
