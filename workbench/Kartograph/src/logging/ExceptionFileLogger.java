package logging;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;

public class ExceptionFileLogger {
	
	public static final ExceptionFileLogger LOGGER = ExceptionFileLogger.createInstance("log.txt");
	
	private FileOutputStream fs;
	private DataOutputStream dos;
	private PrintStream ps;
	private ByteArrayOutputStream baos;

	private ExceptionFileLogger(String filename) throws FileNotFoundException {
		fs = new FileOutputStream(new File(filename));
		dos = new DataOutputStream(fs);
		baos = new ByteArrayOutputStream();
		ps = new PrintStream(baos);
		try {
			dos.writeUTF("New log\n");
		} catch (IOException e) {
			System.out.println("could not start file");
			e.printStackTrace();
		}
	}
	
	public static ExceptionFileLogger createInstance(String name){
		try {
			return new ExceptionFileLogger(name);
		} catch (FileNotFoundException e) {
			System.out.println("Could not create filelogger!");
			e.printStackTrace();
			return null;
		}
	}
	
	public void writeString(String str){
		try {
			dos.writeUTF(str+"\n");
			dos.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void writeException(Exception e){
		writeString("Ex: "+e.getMessage());
		
		e.printStackTrace(ps);
		ps.flush();
		
		try {
			dos.writeUTF(baos.toString()+"\n" );
		} catch (IOException e1) {
			System.out.println("ERR: write ex");
			e1.printStackTrace();
		}
	}
	
	public void close(){
		baos.close();
		try {
			fs.close();
		} catch (IOException e) {
			System.out.println("ERR: close");
			e.printStackTrace();
		}
	}

}
