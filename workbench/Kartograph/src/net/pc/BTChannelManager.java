package net.pc;

import java.util.Iterator;
import java.util.Set;
import java.util.TreeMap;

import lejos.pc.comm.NXTComm;
import lejos.pc.comm.NXTCommException;
import lejos.pc.comm.NXTCommFactory;
import lejos.pc.comm.NXTInfo;
import logic.PCControler;
import net.pc.commands.AbstractCommand;

/**
 * Class to manage several BTChannels
 * 
 * @author Loris
 * 
 */
public class BTChannelManager {
	public static final String NXT_NAME = "NXT";

	private TreeMap<Integer, BTChannel> channels;
	
	private int lastID = 0;

	private NXTComm comm;
	
	private PCControler master;

	public BTChannelManager(PCControler ctrl) {
		this.master = ctrl;
		this.channels = new TreeMap<Integer, BTChannel>();
		try {
			System.out.println("Trying to create NXTComm");
			this.comm = NXTCommFactory.createNXTComm(NXTCommFactory.BLUETOOTH);
			System.out.println("Established Comm");
		} catch (NXTCommException e) {
			System.err.println("Failed in buiding the NXTComm");
			e.printStackTrace();
		}
		
		findAndConnect(NXT_NAME);
	}
	
	
	private void findAndConnect(String name){
		int lastID = -1;
		NXTInfo[] infos = null;
		try {
			System.out.println("searching for connections");
			infos = comm.search(name);
			System.out.println("Found informations");
		} catch (NXTCommException e) {
			System.err.println("Failed in searching for: " + name);
			e.printStackTrace();
		}
		System.out.println("bla");//DEBUG
		if (infos != null) {
			System.out.println("alb "+infos.length);//DEBUG
			for (NXTInfo nfo : infos) {
				System.out.println("asdf");//DEBUG
				if (nfo != null) {
					System.out.println("hi");//DEBUG
					boolean succ = false;
					try {
						lastID = ++this.lastID;
						channels.put(lastID, new BTChannel(comm, nfo, master));
						succ = true;
					} catch (NXTCommException e) {
						System.err.println("Could not create a BTChannel for "
								+ nfo.name);
						e.printStackTrace();
					}
					System.out.println("Creted new channel: "+succ);
					if(succ){
						Thread t = new Thread(channels.get(lastID));
						t.start();
					}
				}else{
					System.err.println("CUrrent nxtinfo is null");
				}

			}
		}else{
			System.err.println("No NXTInfos found it is null");
		}
	}
	
	public int[] getIDs(){
		Set<Integer> keys = channels.keySet();
		int[] out = new int[keys.size()];
		int c = 0;
		Iterator<Integer> it = keys.iterator();
		while(it.hasNext()){
			out[c++] = it.next();
		}
		return out;
	}
	
	public void putCommand(AbstractCommand cmd, int ID) throws InterruptedException{
		channels.get(ID).addCommand(cmd);
	}

}
