package logging;

/**
 * Simple logger that logs to the console.
 * This logger logs only messages greater or equal than the set logging level.gi
 * @author loris.sauter
 * @version 0.1
 */
public class ConsoleLogger extends AbstractLogger {
	
	ConsoleLogger(String name, byte level){
		super(name, level);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void log(String msg) {
		log(super.getLevel(), msg);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void debug(String msg) {
		log(LogConfig.DEBUG_LEVEL, msg);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void info(String msg) {
		log(LogConfig.INFO_LEVEL, msg);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void warn(String msg) {
		log(LogConfig.WARN_LEVEL, msg);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void error(String msg) {
		log(LogConfig.ERROR_LEVEL, msg);
	}

	/**
	 * Logs a message on given level if the given level is greater or equal than the set level.
	 * @param level The level of the message
	 * @param msg The log message.
	 */
	private void log(byte level, String msg){
		if(level >= super.getLevel()){
			System.out.println(LogConfig.formatEasy(this, msg));
		}
	}
}
