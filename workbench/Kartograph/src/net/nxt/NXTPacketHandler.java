/**
 * 
 */
package net.nxt;

import net.nxt.commands.CommandException;
import net.packets.AbstractPacketHandler;
import net.packets.DataPacket;
import net.packets.Packet;
import net.packets.commands.CommandPacket;

/**
 * Provides basic packet handling on an nxt brick.
 * @author loris.sauter
 *
 */
public class NXTPacketHandler extends AbstractPacketHandler {
	
	private CommandInterpreter interpreter;
	
	public NXTPacketHandler(){
		super();
		interpreter = new NXTCmdInterpreter(this);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean onPacketReceived(Packet packet) {
		System.out.println("Rec: "+packet.getId());
		if(packet instanceof CommandPacket){
			try {
				return interpreter.interpretCommand((CommandPacket)packet);
			} catch (ClosedConnectionException e) {
				return false;
			} catch (CommandException e) {
				System.out.println(e.getMessage());
				e.printStackTrace();
				return false;
			}
		}else if(packet instanceof DataPacket<?>){
			System.out.println("Received: "+packet.getId()+" with content: "+ ((DataPacket<?>)packet).get());
			return true;
		}else{
			//not datapacket: "object" packet
			System.out.println("Pkt: obj" );
		}
		return false;
	}

}
