/**
 * 
 */
package net.pc.commands;

import net.packets.Packet;
import net.packets.commands.CloseCmdPacket;

/**
 * Tells the nxt brick to close the connection
 * @author loris.sauter
 *
 */
public class CloseCommand extends AbstractCommand {

	/**
	 * Creates a new CloseCommand
	 */
	public CloseCommand() {
		super(new CloseCmdPacket() );
	}

	/** 
	 * {@inheritDoc}
	 * @throws ClosingConnectionException 
	 * @see net.pc.commands.AbstractCommand#onReceive(net.packets.Packet)
	 */
	@Override
	public boolean onReceive(Packet answer) throws UnexpectedAnswerException{
		try {
			super.onClose();
		} catch (ClosingConnectionException e) {
			return true;
		}
		return false;
	}

}
