/**
 * 
 */
package net.packets;

/**
 * An exception to indicate that the PacketHandler was not correctly setup.
 * @author loris.sauter
 * @version 1.0
 */
public class NotSetupException extends Exception {
	
	private final static String defaultMessage = "The PacketHandler had either no DataInputStream or not DataOutputStream attached";

	/**
	 * Creates a new NotSetupException with the default message.
	 */
	public NotSetupException() {
		super(defaultMessage);
	}

	/**
	 * Creates a NotSetupException with custom message.
	 * @param message
	 */
	public NotSetupException(String message) {
		super(message);
	}

}
