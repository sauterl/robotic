package net.packets;

public class PacketException extends RuntimeException {
	
	public PacketException(){
		super();
	}
	
	public PacketException(String msg){
		super(msg);
	}

}
