package control;

import java.util.ArrayList;

import lejos.geom.Point;

public class MesureMap {
	private static final int RESOLUTION = 25;
	// private static final int RESOLUTION = 1;
	public boolean[][] mesured;
	public boolean[][] tmpunev;
	public boolean[][] tunreach;
	public boolean[][] unreachabel;
	private int r2X = 10000;
	private int r2Y = 10000;
	private Point startPosition;

	public MesureMap(int size) {
		size = size / RESOLUTION;
		mesured = new boolean[size][size];
		unreachabel = new boolean[size][size];
		tmpunev = new boolean[size][size];
		tunreach = new boolean[size][size];
		startPosition = new Point(0, 0);
	}

	public MesureMap(int size, Point startPosition) {
		size = size / RESOLUTION;
		mesured = new boolean[size][size];
		unreachabel = new boolean[size][size];
		tmpunev = new boolean[size][size];
		tunreach = new boolean[size][size];
		this.startPosition = startPosition;
	}

	public void forceunreach(Point p) {
		unreachabel[resolveX(p)][resolveY(p)] = true;
	}

	public Point getNearest2(Point p) {
		int x, y;
		x = resolveX(p);
		y = resolveY(p);
		unreachabel[x][y] = false;
		if (test(x, y)) {
			return new Point(getCoordinateX(x), getCoordinateY(y));
		}
		resetTempunev();
		int ind = 0;

		ArrayList<Pair> parents = new ArrayList<Pair>();

		// parents.add(new Pair(x,y));

		parents.add(new Pair(x + 1, y));
		parents.add(new Pair(x, y + 1));
		parents.add(new Pair(x - 1, y));
		parents.add(new Pair(x, y - 1));

		tmpunev[x][y] = true;
		while (checkFinish()) {
			if (ind < parents.size()) {
				if (!mesured[parents.get(ind).x][parents.get(ind).y]
						&& !tunreach[parents.get(ind).x][parents.get(ind).y] && !(x == r2X && y == r2Y)) {
					break;
				} else {
					ind++;
				}
			} else {
				parents = getGoodNeighbours(parents);
				if (parents.size() == 0) {
					System.out.println("finished");
					break;
				}
				ind = 0;
			}

		}

		return getMesp(parents.get(ind).x, parents.get(ind).y);

		// return new Point(getCoordinateX(parents.get(ind).x),getCoordinateY(parents.get(ind).y));
	}

	// public Point getNearest(Point p) {
	// int x, y;
	// x = resolveX(p);
	// y = resolveY(p);
	// if (test(x, y)) {
	// return new Point(getCoordinateX(x), getCoordinateY(y));
	// }
	// int i = 0;
	// int dist = 0;
	// while (i < Math.pow(mesured.length, 2)) {
	// dist++;
	// int xt = x - dist;
	// int yt = y - dist;
	// while (xt < x + dist) {
	// if (test(xt++, yt)) {
	// return new Point(getCoordinateX(xt), getCoordinateY(yt));
	// }
	// i++;
	// }
	// while (yt < +dist) {
	// if (test(xt, yt++)) {
	// return new Point(getCoordinateX(xt), getCoordinateY(yt));
	// }
	// i++;
	// }
	// while (xt > x - dist) {
	// if (test(xt--, yt)) {
	// return new Point(getCoordinateX(xt), getCoordinateY(yt));
	// }
	// i++;
	// }
	// while (yt > y - dist + 1) {
	// if (test(xt, yt--)) {
	// return new Point(getCoordinateX(xt), getCoordinateY(yt));
	// }
	// i++;
	// }
	// }
	//
	// return null;
	// }

	public Point getNearestPC(Point p) {
		int x, y;
		x = resolveX(p);
		y = resolveY(p);
		unreachabel[x][y] = false;
		if (test(x, y)) {
			return new Point(getCoordinateX(x), getCoordinateY(y));
		}
		resetTempunev();
		int ind = 0;

		ArrayList<Pair> parents = new ArrayList<Pair>();

		// parents.add(new Pair(x,y));

		parents.add(new Pair(x + 1, y));
		parents.add(new Pair(x, y + 1));
		parents.add(new Pair(x - 1, y));
		parents.add(new Pair(x, y - 1));

		tmpunev[x][y] = true;
		while (checkFinish()) {
			if (ind < parents.size()) {
				if (!mesured[parents.get(ind).x][parents.get(ind).y]
						&& !tunreach[parents.get(ind).x][parents.get(ind).y] && !(x == r2X && y == r2Y)) {
					break;
				} else {
					ind++;
				}
			} else {
				parents = getGoodNeighbours2(parents);
				if (parents.size() == 0) {
					System.out.println("finished");
					break;
				}
				ind = 0;
			}

		}

		return getMespPC(parents.get(ind).x, parents.get(ind).y);

		// return new Point(getCoordinateX(parents.get(ind).x),getCoordinateY(parents.get(ind).y));
	}

	public Point getR2() {
		return new Point(getCoordinateX(r2X), getCoordinateY(r2Y));
	}

	public int getSize() {
		return mesured.length * RESOLUTION;
	}

	// public float getR2X() {
	// return getCoordinateX(r2X);
	// }

	// public void setR2X(float r2x) {
	// r2X = resolveX(r2x);
	// }
	// public float getR2Y() {
	// return getCoordinateY(r2Y);
	// }

	public boolean getStatus(Point p) {
		return unreachabel[resolveX(p)][resolveY(p)];
	}

	public void mesured(Point p) {
		int x = resolveX(p);
		int y = resolveY(p);
		try {
			mesured[x][y] = true;
			// unreachabel[x][y]=false;
			// System.out.println("X: "+x+"\tX: "+y+"\tmesured");
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("X: " + x + "\tX: " + y + "\tmesured but out of bounds");
			System.exit(-1);
		}
		mesuredSurrounding(x, y);
		// mesuredSurrounding(x+1,y+1);
		mesuredSurrounding(x + 1, y);
		// mesuredSurrounding(x+1,y-1);
		mesuredSurrounding(x, y - 1);
		// mesuredSurrounding(x-1,y+1);
		mesuredSurrounding(x - 1, y);
		// mesuredSurrounding(x-1,y-1);
		mesuredSurrounding(x, y + 1);
	}

	public void printState(Point p) {
		int x = resolveX(p);
		int y = resolveY(p);
		System.out.println("mesured: " + mesured[x][y] + "   unreachabel: " + unreachabel[x][y]);
		System.out.println();
		for (int i = 0; i < mesured.length; i++) {
			for (int j = 0; j < mesured.length; j++) {
				if (i == x && j == y) {
					System.out.print("R ");
				} else {
					if (mesured[i][j] && !unreachabel[i][j]) {
						System.out.print("G ");
					} else if (unreachabel[i][j]) {
						System.out.print("H ");
					} else {
						// System.out.print("U ");
						System.out.print("  ");
					}
				}
			}
			System.out.println();
		}

	}

	public void resetTempunev() {
		tmpunev = new boolean[mesured.length][mesured[0].length];
	}

	public void resetTmpunreach() {
		tunreach = new boolean[mesured.length][mesured[0].length];
	}

	// public void setR2Y(int r2y) {
	// r2Y = r2y;
	// }
	public void setR2(Point positionR2) {
		r2X = resolveX(positionR2);
		r2Y = resolveY(positionR2);
	}

	// public void setTempunev(Point p) {
	// // int x=resolveX(p);
	// // int y=resolveY(p);
	// // tmpunev[x][y]=true;
	// }

	public void setTempUnreach(Point p) {
		int x = resolveX(p);
		int y = resolveY(p);
		tunreach[x][y] = true;
	}

	// public Point retPoint(int x, int y){
	// if(!mesured[x][y]&&!unreachabel[x][y]){
	// return new Point(getCoordinateX(x),getCoordinateY(y));
	// }
	//
	// }

	public void unreach(Point p) {
		int x = resolveX(p);
		int y = resolveY(p);
		unreach(x, y);
		// unreach(x-1,y-1);
		// unreach(x,y-1);
		// unreach(x+1,y-1);
		// unreach(x+1,y);
		// unreach(x+1,y+1);
		// unreach(x,y+1);
		// unreach(x-1,y+1);
		// unreach(x-1,y);
	}

	// public void unreachB(Point p) {
	// int x = resolveX(p);
	// int y = resolveY(p);
	// try {
	// unreachabel[x][y] = true;
	// // System.out.println("X: "+x+"\tY: "+y+"\tunreachabel");
	// } catch (ArrayIndexOutOfBoundsException e) {
	// System.out.println("X: " + x + "\tY: " + y + "\tunreachabel but out of bounds");
	// }
	//
	// }

	private boolean checkFinish() {
		return true;
	}

	private float getCoordinateX(int x) {
		// System.out.println("out X: "+(x-mesured.length/2)*RESOLUTION);
		return (x - mesured.length / 2) * RESOLUTION;
	}

	private float getCoordinateY(int y) {
		// System.out.println("\tout Y: "+(y-mesured[0].length/2)*RESOLUTION);
		return (y - mesured[0].length / 2) * RESOLUTION;
	}

	private ArrayList getGoodNeighbours(ArrayList<Pair> parents) {
		ArrayList<Pair> pairs = new ArrayList<Pair>();
		for (int i = 0; i < parents.size(); i++) {
			// if(testGood(parents.get(i).x-1,parents.get(i).y-1)){
			// pairs.add(new Pair(parents.get(i).x-1,parents.get(i).y-1));
			// }
			if (testGood(parents.get(i).x, parents.get(i).y - 1)) {
				pairs.add(new Pair(parents.get(i).x, parents.get(i).y - 1));
			}
			// if(testGood(parents.get(i).x+1,parents.get(i).y-1)){
			// pairs.add(new Pair(parents.get(i).x+1,parents.get(i).y-1));
			// }
			if (testGood(parents.get(i).x + 1, parents.get(i).y)) {
				pairs.add(new Pair(parents.get(i).x + 1, parents.get(i).y));
			}
			// if(testGood(parents.get(i).x+1,parents.get(i).y+1)){
			// pairs.add(new Pair(parents.get(i).x+1,parents.get(i).y+1));
			// }
			if (testGood(parents.get(i).x, parents.get(i).y + 1)) {
				pairs.add(new Pair(parents.get(i).x, parents.get(i).y + 1));
			}
			// if(testGood(parents.get(i).x-1,parents.get(i).y+1)){
			// pairs.add(new Pair(parents.get(i).x-1,parents.get(i).y+1));
			// }
			if (testGood(parents.get(i).x - 1, parents.get(i).y)) {
				pairs.add(new Pair(parents.get(i).x - 1, parents.get(i).y));
			}
		}
		return pairs;
	}

	private ArrayList getGoodNeighbours2(ArrayList<Pair> parents) {
		ArrayList<Pair> pairs = new ArrayList<Pair>();
		for (int i = 0; i < parents.size(); i++) {
			if (testGood(parents.get(i).x - 1, parents.get(i).y - 1)) {
				pairs.add(new Pair(parents.get(i).x - 1, parents.get(i).y - 1));
			}
			if (testGood(parents.get(i).x, parents.get(i).y - 1)) {
				pairs.add(new Pair(parents.get(i).x, parents.get(i).y - 1));
			}
			if (testGood(parents.get(i).x + 1, parents.get(i).y - 1)) {
				pairs.add(new Pair(parents.get(i).x + 1, parents.get(i).y - 1));
			}
			if (testGood(parents.get(i).x + 1, parents.get(i).y)) {
				pairs.add(new Pair(parents.get(i).x + 1, parents.get(i).y));
			}
			if (testGood(parents.get(i).x + 1, parents.get(i).y + 1)) {
				pairs.add(new Pair(parents.get(i).x + 1, parents.get(i).y + 1));
			}
			if (testGood(parents.get(i).x, parents.get(i).y + 1)) {
				pairs.add(new Pair(parents.get(i).x, parents.get(i).y + 1));
			}
			if (testGood(parents.get(i).x - 1, parents.get(i).y + 1)) {
				pairs.add(new Pair(parents.get(i).x - 1, parents.get(i).y + 1));
			}
			if (testGood(parents.get(i).x - 1, parents.get(i).y)) {
				pairs.add(new Pair(parents.get(i).x - 1, parents.get(i).y));
			}
		}
		return pairs;
	}

	private Point getMesp(int x, int y) {
		try {
			if (mesured[x + 1][y] && !unreachabel[x + 1][y]) {
				return new Point(getCoordinateX(x + 1), getCoordinateY(y));
			}
		} catch (ArrayIndexOutOfBoundsException e) {

		}
		try {
			if (mesured[x][y + 1] && !unreachabel[x][y + 1]) {
				return new Point(getCoordinateX(x), getCoordinateY(y + 1));
			}
		} catch (ArrayIndexOutOfBoundsException e) {

		}
		try {
			if (mesured[x - 1][y] && !unreachabel[x - 1][y]) {
				return new Point(getCoordinateX(x - 1), getCoordinateY(y));
			}
		} catch (ArrayIndexOutOfBoundsException e) {

		}
		try {
			if (mesured[x][y - 1] && !unreachabel[x][y - 1]) {
				return new Point(getCoordinateX(x), getCoordinateY(y - 1));
			}
		} catch (ArrayIndexOutOfBoundsException e) {

		}
		return new Point(getCoordinateX(x), getCoordinateY(y - 1));
		// return startPosition;
	}

	private Point getMespPC(int x, int y) {
		try {
			if (mesured[x + 1][y] && !unreachabel[x + 1][y]) {
				return new Point(getCoordinateX(x + 1), getCoordinateY(y));
			}
		} catch (ArrayIndexOutOfBoundsException e) {

		}
		try {
			if (mesured[x][y + 1] && !unreachabel[x][y + 1]) {
				return new Point(getCoordinateX(x), getCoordinateY(y + 1));
			}
		} catch (ArrayIndexOutOfBoundsException e) {

		}
		try {
			if (mesured[x - 1][y] && !unreachabel[x - 1][y]) {
				return new Point(getCoordinateX(x - 1), getCoordinateY(y));
			}
		} catch (ArrayIndexOutOfBoundsException e) {

		}
		try {
			if (mesured[x - 1][y - 1] && !unreachabel[x - 1][y - 1]) {
				return new Point(getCoordinateX(x - 1), getCoordinateY(y - 1));
			}
		} catch (ArrayIndexOutOfBoundsException e) {

		}
		try {
			if (mesured[x + 1][y - 1] && !unreachabel[x + 1][y - 1]) {
				return new Point(getCoordinateX(x + 1), getCoordinateY(y - 1));
			}
		} catch (ArrayIndexOutOfBoundsException e) {

		}
		try {
			if (mesured[x - 1][y + 1] && !unreachabel[x - 1][y + 1]) {
				return new Point(getCoordinateX(x - 1), getCoordinateY(y + 1));
			}
		} catch (ArrayIndexOutOfBoundsException e) {

		}

		return new Point(getCoordinateX(x + 1), getCoordinateY(y + 1));
	}

	private void mesuredSurrounding(int x, int y) {

		try {
			mesured[x][y] = true;
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("mesured but out of bounds");
		}
		// System.out.println(x+" "+y+" "+mesured[x][y]);
	}

	private int resolveX(Point p) {
		// System.out.println("Point in"+ p);
		return mesured.length / 2 + (int) p.x / RESOLUTION;
	}

	private int resolveY(Point p) {
		// System.out.println("Point in"+ p);
		return mesured[0].length / 2 + (int) p.y / RESOLUTION;
	}

	private boolean test(int x, int y) {
		try {
			// System.out.println(mesured[x][y]+" "+unreachabel[x][y]);
			return !mesured[x][y] && !unreachabel[x][y];
		} catch (ArrayIndexOutOfBoundsException e) {
			return false;
		}

	}

	private boolean testGood(int x, int y) {
		try {
			boolean ret = !tmpunev[x][y] && !unreachabel[x][y]/* &&!tunreach[x][y] */;
			if (!tmpunev[x][y]) {
				tmpunev[x][y] = true;
			}
			return ret;
		} catch (ArrayIndexOutOfBoundsException e) {
			return false;
		}
	}

	private void unreach(int x, int y) {
		try {
			if (!mesured[x][y]) {
				unreachabel[x][y] = true;
			}
			// System.out.println("X: "+x+"\tY: "+y+"\tunreachabel");
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("X: " + x + "\tY: " + y + "\tunreachabel but out of bounds");
		}
	}
}
