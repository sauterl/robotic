/**
 * 
 */
package net.packets.commands;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import lejos.geom.Point;
import net.packets.PacketIDRegistry;

/**
 * @author loris.sauter
 *
 */
public class CmdInitPacket implements CommandPacket {
	
	private Point start;
	private int size;
	
	public CmdInitPacket(){}
	public CmdInitPacket(int size, Point start){
		this.size = size;
		this.start = start;
	}

	public Point getStart() {
		return start;
	}

	public void setStart(Point start) {
		this.start = start;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	/** 
	 * {@inheritDoc}
	 * @see net.packets.Packet#getId()
	 */
	@Override
	public int getId() {
		return PacketIDRegistry.CMD_INIT;
	}

	/** 
	 * {@inheritDoc}
	 * @see net.packets.Packet#write(java.io.DataOutputStream)
	 */
	@Override
	public void write(DataOutputStream to) throws IOException {
		to.write(this.getId() );
		to.writeInt(size);
		to.writeFloat(start.x);
		to.writeFloat(start.y);
		to.flush();

	}

	/** 
	 * {@inheritDoc}
	 * @see net.packets.Packet#read(java.io.DataInputStream)
	 */
	@Override
	public void read(DataInputStream from) throws IOException {
		size = from.readInt();
		start = new Point(0,0);
		start.x = from.readFloat();
		start.y = from.readFloat();
	}

	/** 
	 * {@inheritDoc}
	 * @see net.packets.commands.CommandPacket#executeCommand()
	 */
	@Override
	public void executeCommand() {
	}

}
