/**
 * This package contains useful classes to get a simple and basic logging API.
 */
/**
 * @author loris.sauter
 * @version 0.1
 *
 */
package logging;