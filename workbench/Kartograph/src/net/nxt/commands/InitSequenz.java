/**
 * 
 */
package net.nxt.commands;

import java.io.IOException;

import lejos.geom.Point;
import net.packets.NotSetupException;
import net.packets.Packet;
import net.packets.commands.CmdInitPacket;
import net.packets.commands.OKCmdPacket;

/**
 * @author loris.sauter
 *
 */
public class InitSequenz extends CommandSequenz {
	
	private int size;
	private Point start;

	/** 
	 * {@inheritDoc}
	 * @see net.nxt.commands.CommandSequenz#executeSequenz(net.packets.Packet)
	 */
	@Override
	public boolean executeSequenz(Packet packet) throws SequenzException {
		if(packet instanceof CmdInitPacket){
			this.size = ((CmdInitPacket) packet).getSize();
			this.start = ((CmdInitPacket) packet).getStart();
			this.interpreter.createController(size, start);
			try {
				handler.send(new OKCmdPacket());
			} catch (NotSetupException | IOException e) {
				e.printStackTrace();
			}
		}
		return false;
	}

}
