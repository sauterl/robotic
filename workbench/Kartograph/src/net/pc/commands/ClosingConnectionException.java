/**
 * 
 */
package net.pc.commands;

/**
 * An exception to indicate that the connection is going to get closed.
 * 
 * @author loris.sauter
 *
 */
public class ClosingConnectionException extends Exception{

	/**
	 * Creates a new ClosingConnectionException with the default message.
	 */
	public ClosingConnectionException() {
		super("Got closing requested...");
	}
	
	/**
	 * Creates a new ClosingConnectionException with custom message.
	 * @param msg The custom message to describe this exception
	 */
	public ClosingConnectionException(String msg){
		super(msg);
	}

}
