Those .launch files are
_eclipse launch configurations_ which are set to:

 * Compile (also packages) to <projectdir>/bin/
 * Download (also packages) from <projectdir>/bin/
 
Copy them to:

<workspace>/.metadata/.plugins/.org.eclipse.debug.core/.launches