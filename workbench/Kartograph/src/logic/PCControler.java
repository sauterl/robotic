package logic;

import lejos.geom.Point;
import lejos.robotics.navigation.DestinationUnreachableException;
import lejos.robotics.pathfinding.Path;
import vectormap.Chunk;
import vectormap.Line;
import control.MesureMap;

public class PCControler {
	private Chunk ch;
	private MesureMap mesMap;
	private int size;
	private Line[] tmplines = null;

	public PCControler(int size) {
		this.size = size;
		mesMap = new MesureMap(size);
		ch = new Chunk(size, mesMap);
	}

	public Path confdirmNextPoint(Point position, Point next, Point r2) {
		if (mesMap.getStatus(next)) {
			System.out.println("corrected Point");
			try {
				return ch.findWay(position, mesMap.getNearest2(next),r2);
			} catch (DestinationUnreachableException e) {
				try {
					return ch.findWay(position, new Point(0,0), new Point(1000,1000));
				} catch (DestinationUnreachableException e1) {
					System.exit(-1);
				}
			}
		}
		try {
			return ch.findWay(position, next, r2);
		} catch (DestinationUnreachableException e) {
			try {
				return ch.findWay(position, new Point(0,0), new Point(1000,1000));
			} catch (DestinationUnreachableException e1) {
				System.exit(-1);
			}
		}
		return null;
	}

	public Chunk getCh() {
		return ch;
	}

	public MesureMap getMesMap() {
		return mesMap;
	}

	public int getSize() {
		return size;
	}

	public void mergeMesureMap(boolean[][] mesured, boolean[][] unreach) {
		for (int x = 0; x < unreach.length; x++) {
			for (int y = 0; y < unreach.length; y++) {
				if (unreach[x][y] || mesMap.unreachabel[x][y]) {
					mesMap.unreachabel[x][y] = true;
				}
				if (mesured[x][y] || mesMap.mesured[x][y]) {
					mesMap.mesured[x][y] = true;
				}
			}
		}
	}

	public boolean mergeVectorMap(Line[] lines) {
		if (tmplines == null) {
			tmplines = lines;
			return false;
		} else {
			ch.exchangeLines(tmplines);
			for (Line l : lines) {
				ch.addLine(l);
			}
			tmplines = null;
			return true;
		}
	}

}
