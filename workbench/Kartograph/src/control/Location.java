package control;

import lejos.geom.Point;
import lejos.nxt.SensorPort;
import lejos.nxt.addon.CompassHTSensor;

/**
 * This class is used to hold track of the robots position in the cartesian coordinate system.
 * 
 * @author Eddie
 *
 */
public class Location {
	/**
	 * compass needed for the accurate direction. it must be pluged in on the {@link SensorPort}.S2.
	 */
	private CompassHTSensor compass;
	/**
	 * x and y coordinate of the current position
	 */
	private double x, y;

	/**
	 * Default constructor. Initializes the {@code Location} object with the coordinate 0 0,
	 */
	// public Location() {
	// this.x = 0;
	// this.y = 0;
	// compass = new CompassHTSensor(SensorPort.S2);
	// }

	/**
	 * Constructor with the possibility to specify the starting coordinates.
	 * 
	 * @param x x-coordinate in centimeters
	 * @param y y-coordinate in centimeters
	 */
	public Location(double x, double y) {
		this.x = x;
		this.y = y;
		compass = new CompassHTSensor(SensorPort.S2);
	}

	/**
	 * Get the angle of the current position to a other point in the cartesian coordinate system.
	 * 
	 * @param x x-coordinate of the second position
	 * @param y y-coordinate of the second position
	 * @return degrees in the cartesian coordinate system.
	 */
	public float getAngle(double x, double y) {
		if (x == 0) {
			if (y > 0) {
				return 90;
			} else {
				return 270;
			}
		}
		if (y == 0) {
			if (x > 0) {
				return 0;
			} else {
				return 180;
			}
		}
		if (x > 0 && y > 0) {
			return radDeg(Math.acos(x / Math.sqrt(x * x + y * y)));
		}
		if (x < 0 && y > 0) {
			return radDeg(Math.acos(y / Math.sqrt(x * x + y * y))) + 90;
		}
		if (x < 0 && y < 0) {
			return radDeg(Math.acos(-x / Math.sqrt(x * x + y * y))) + 180;
		}
		if (x > 0 && y < 0) {
			return radDeg(Math.acos(-y / Math.sqrt(x * x + y * y))) + 270;
		}
		return 0;

	}

	/**
	 * Get the current direction the compass is pointing to.
	 * 
	 * @return degrees in the cartesian coordinate system.
	 */
	public float getDirection() {
		return compass.getDegreesCartesian();

	}

	public Point getPosition() {
		return new Point((float) x, (float) y);
	}

	public Point getPositionC() {
		return new Point((float) getXC(), (float) getYC());
	}

	/**
	 * get the angle between the current direction of the compass and a given coordinate in the cartesian coordinate
	 * system. This method will allways return the shortest angle to turn to the given coordinate.
	 * 
	 * @param x x-coordinate of the point
	 * @param y y-coordinate of the point
	 * @return degrees between -180 and 180 needed to turn to the given coordinate.
	 */
	public float getRelAngle(double x, double y) {
		double angle = getAngle(x, y);
		return getRelAngle((float) angle);
	}

	/**
	 * get the angle between the current direction of the compass and a given angle in the cartesian coordinate system.
	 * This method will allways return the shortest angle to turn to the given angle.
	 * 
	 * @param angle cartesian angle to return the shortest angle to
	 * @return degrees between -180 and 180 needed to turn to the given angle.
	 */
	public float getRelAngle(float angle) {
		float curangle = getDirection();
		int relAngle = (int) (angle - curangle);
		relAngle = (relAngle) % 360;
		if (Math.abs(relAngle) < 180) {
			return (relAngle);
		} else {
			if (relAngle < 0) {
				return (360 + relAngle) % 360;
			} else {
				return -1 * (360 - relAngle) % 360;
			}
		}
	}

	/**
	 * Get the distance between the current position and a given point
	 * 
	 * @param x x-coordinate of the point
	 * @param y y-coordinate of the point
	 * @return
	 */
	public double getRelDistance(double x, double y) {
		return Math.sqrt(Math.pow(this.x - x, 2) + Math.pow(this.y - y, 2));
	}

	/**
	 * Get the x-coordinate of the current position.
	 * 
	 * @return x-coordinate in centimeters
	 */
	public double getX() {
		return x;
	}

	public double getXC() {
		return x + Math.cos(degRad(getDirection())) * 8.5;
	}

	/**
	 * Get the y-coordinate of the current position.
	 * 
	 * @return y-coordinate in centimeters
	 */
	public double getY() {
		return y;
	}

	public double getYC() {
		return y - Math.sin(degRad(getDirection())) * 8.5;
	}

	/**
	 * Move the current position a specified distance in the direction the compass is pointing to.
	 * 
	 * @param distance distance in centimeters
	 */
	// @Deprecated
	// public void move(double distance) {
	// x += distance * Math.cos(getDirection() / 180 * Math.PI);
	// y += distance * Math.sin(getDirection() / 180 * Math.PI);
	// }

	// /**
	// * Move the current position a specified distance in x direction.
	// *
	// * @param distance distance in centimeters
	// */
	// public void moveX(double distance) {
	// x += distance;
	// }

	// /**
	// * Move the current position a specified distance in y direction.
	// *
	// * @param distance distance in centimeters
	// */
	// public void moveY(double distance) {
	// y += distance;
	// }

	/**
	 * Set the x-coordinate of the current position.
	 * 
	 * @param x x-coordinate in centimeters
	 */
	public void setX(double x) {
		this.x = x;
	}

	/**
	 * Set the y-coordinate of the current position.
	 * 
	 * @param y y-coordinate in centimeters
	 */
	public void setY(double y) {
		this.y = y;
	}

	/**
	 * start the calibration of the compass.
	 */
	public void startCal() {
		compass.startCalibration();
	}

	/**
	 * stop the calibration of the compass.
	 */
	public void stopCal() {
		compass.stopCalibration();
	}

	private float degRad(double deg) {
		return (float) (deg / 180 * Math.PI);
	}

	/**
	 * Method to convert from radians to degrees
	 * 
	 * @param rad angle in radians
	 * @return angle in degrees
	 */
	private float radDeg(double rad) {
		return (float) (rad / Math.PI * 180);
	}
}
