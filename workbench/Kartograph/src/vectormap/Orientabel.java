package vectormap;

/**
 * Interface for classes that have a direction. Used to make stuff more exchangabel.
 * 
 * @author Eddie
 *
 */
public interface Orientabel {
	/**
	 * returns the direction something is pointing to.
	 * 
	 * @return direction in degrees between 0 and 180
	 */
	public float direction();

	public float getEndX();

	public float getEndY();

	public float getStartX();

	public float getStartY();

	public float lenght();

	public void overwrite(float startX, float startY, float endX, float endY);
}
