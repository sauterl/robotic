package logic;

import lejos.geom.Point;
import lejos.robotics.navigation.DestinationUnreachableException;
import lejos.robotics.pathfinding.Path;
import mesure.Mesure;
import mesure.MesureException;
import vectormap.Chunk;
import vectormap.Line;
import control.MesureMap;
import control.RobotAboutToCrashException;
import control.RobotControl;

public class NXTControler {

	private Chunk ch;

	private RobotControl contr;

	private Mesure mes;

	private MesureMap mesMap;
	private int size;

	public NXTControler() {
		initStartPosition(new Point(0, 0));
		initSize(500);
		initRest();
	}

	public NXTControler(int size) {
		initStartPosition(new Point(0, 0));
		initSize(size);
		initRest();
	}

	public NXTControler(int size, Point startPosition) {
		initStartPosition(startPosition);
		initSize(size);
		initRest();
	}

	public Point drive( Path way) {
//		mesMap.setR2(r2);

//		try {
//			way = ch.findWay(contr.getLocation(), aim, r2);
//		} catch (DestinationUnreachableException e) {
//			mesMap.forceunreach(aim);
//			return getPosition();
//		}

		try {
			contr.drive(way);
		} catch (RobotAboutToCrashException e) {
//			way.get(way.size()-1);
			mesMap.setTempUnreach(way.get(way.size()-1));
			mes.forceUnreach(mesMap);
			mesMap.forceunreach(way.get(way.size()-1));
			return getPosition();
		}

		mesMap.resetTmpunreach();
		return getPosition();
	}

	public MesureMap getMesMap() {
		return mesMap;
	}

	public Point getNextPoint(boolean[][] mesured, boolean[][] unreach, Point r2) {
		setMesureMap(mesured, unreach);
		mesMap.setR2(r2);
		return mesMap.getNearest2(getPosition());
	}

	public Point getPosition() {
		return contr.getLocation().getPosition();
	}

	public Chunk getVectormap() {
		return ch;
	}

	public boolean mesure() {
		try {
			mes.MesureWithLog360(mesMap);
		} catch (MesureException e1) {
			System.out.println("pleas hold the robot in position in order to mesure!");
			return false;
		}
		// Test code only
		mesMap.printState(getPosition());

		return true;
	}

	public boolean setVectormap(Line[] lines) {
		ch.exchangeLines(lines);
		return true;
	}

	private void initRest() {
		mesMap = new MesureMap(size);
		ch = new Chunk(size, mesMap);
		contr = new RobotControl(mes);
		mes = new Mesure(contr.getLocation(), ch);
		contr.setMes(mes);
	}

	private void initSize(int size) {
		this.size = size;
	}

	private void initStartPosition(Point start) {
	}

	private void setMesureMap(boolean[][] mesured, boolean[][] unreach) {
		mesMap.mesured = mesured;
		mesMap.unreachabel = unreach;
	}

}
