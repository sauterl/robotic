/**
 * 
 */
package logger;

/**
 * Declares the logging level.
 * @author Loris
 *
 */
public enum LoggingLevel {

	/**
	 * Log level error: only log errors. (really errors?)
	 */
	ERROR,
	
	/**
	 * Log level warning: logs ERROR and custom warnings and exceptions
	 */
	WARNING,
	
	/**
	 * Log level info: logs WARNING and custom messages
	 */
	INFO,
	
	/**
	 * Log level debug: logs INFO and custom messages
	 */
	DEBUG,
	
	/**
	 * Log level verbose: logs DEBUG and custom messages.
	 */
	VERBOSE;
}
