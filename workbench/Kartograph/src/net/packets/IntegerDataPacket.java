package net.packets;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class IntegerDataPacket implements DataPacket<Integer> {

	private int i;
	
	public IntegerDataPacket(){}
	
	public IntegerDataPacket(int i){
		this.i = i;
	}
	
	@Override
	public int getId() {
		return PacketIDRegistry.INTEGER;
	}

	@Override
	public Integer get() {
		return this.i;
	}

	@Override
	public void set(Integer e) {
		this.i = e;
	}

	@Override
	public void write(DataOutputStream to) throws IOException {
		to.write(this.getId());
		to.writeInt(i);
		to.flush();
	}

	@Override
	public void read(DataInputStream from) throws IOException {
		this.i = from.readInt();
	}

}
