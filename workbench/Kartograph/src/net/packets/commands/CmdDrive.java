/**
 * 
 */
package net.packets.commands;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import lejos.geom.Point;
import net.packets.PacketIDRegistry;

/**
 * A command to drive to a certain point
 * @author loris.sauter
 *
 */
public class CmdDrive implements CommandPacket {
	
	private Point otherRobot;
	private Point toGo;

	/** 
	 * {@inheritDoc}
	 * @see net.packets.Packet#getId()
	 */
	@Override
	public int getId() {
		return PacketIDRegistry.CMD_DRIVE;
	}

	public CmdDrive(){}
	
	public CmdDrive(Point toGo, Point otherRobot){
		this.toGo = toGo;
		this.otherRobot=otherRobot;
	}
	
	/** 
	 * {@inheritDoc}
	 * @see net.packets.Packet#write(java.io.DataOutputStream)
	 */
	@Override
	public void write(DataOutputStream to) throws IOException {
		to.write(this.getId());
		to.writeFloat(toGo.x);
		to.writeFloat(toGo.y);
		to.writeFloat(otherRobot.x);
		to.writeFloat(otherRobot.y);
		to.flush();
	}

	/** 
	 * {@inheritDoc}
	 * @see net.packets.Packet#read(java.io.DataInputStream)
	 */
	@Override
	public void read(DataInputStream from) throws IOException {
		this.otherRobot = new Point(0,0);
		this.toGo = new Point(0,0);
		toGo.x = from.readFloat();
		toGo.y = from.readFloat();
		otherRobot.x = from.readFloat();
		otherRobot.y = from.readFloat();
	}

	public Point getOtherRobot() {
		return otherRobot;
	}

	public Point getToGo() {
		return toGo;
	}

	/** 
	 * {@inheritDoc}
	 * @see net.packets.commands.CommandPacket#executeCommand()
	 */
	@Override
	public void executeCommand() {

	}

}
