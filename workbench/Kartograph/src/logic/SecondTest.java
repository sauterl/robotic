package logic;

import java.io.FileNotFoundException;

import lejos.geom.Point;
import lejos.robotics.navigation.DestinationUnreachableException;
import lejos.robotics.pathfinding.Path;
import mesure.Mesure;
import mesure.MesureException;
import vectormap.Chunk;
import control.MesureMap;
import control.RobotAboutToCrashException;
import control.RobotControl;

public class SecondTest {

	public static void main(String[] args) {
		MesureMap mesMap = new MesureMap(500);
		Chunk ch = new Chunk(500, mesMap);
		Mesure mes = null;
		RobotControl contr = new RobotControl(mes);
		mes = new Mesure(contr.getLocation(), ch);
		contr.setMes(mes);

		contr.calibrateCompass();

		for (int i = 0; i < 25; i++) {
			Point p = mesMap.getNearest2(contr.getLocation().getPosition());

			Path pa = null;
//			try {
////				pa = ch.findWay(contr.getLocation(), p, null);
//			} catch (DestinationUnreachableException e) {
//				continue;
//			}

			try {
				contr.drive(pa);
				// contr.drive(new Waypoint(p));
			} catch (RobotAboutToCrashException e) {
				try {
					System.out.println("Minline Mesurement");
					System.out.println("Tried: " + p + "\treached: " + contr.getLocation().getPosition());
					// System.out.println(contr.getLocation().getPosition());
					mes.MesureWithLog360(mesMap);
					mesMap.setTempUnreach(p);
					mes.forceUnreach(mesMap);
					mesMap.forceunreach(p);

					// System.out.println("Tried: "+p+"\treached: "+contr.getLocation().getPosition());
					mesMap.printState(contr.getLocation().getPosition());

				} catch (MesureException e1) {
					e1.printStackTrace();
				}
				continue;
			}

			try {
				System.out.println("Normal mesurement");
				System.out.println("Tried: " + p + "\treached: " + contr.getLocation().getPosition());
				mes.MesureWithLog360(mesMap);
				mesMap.resetTmpunreach();
				// System.out.println("Tried: "+p+"\treached: "+contr.getLocation().getPosition());
				mesMap.printState(contr.getLocation().getPosition());
				// meaMap.mes
			} catch (MesureException e) {
				e.printStackTrace();
			}

		}
		FileWriter w = null;
		try {
			w = new FileWriter("map.svg");
			// w = new FileWriter("map.dat");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.out.println("Failed to initiolize the Filewriter.\n\r" + e.getMessage());
		}
		ch.writeSVG(w);
		// ch.writeData(w);
	}

}
