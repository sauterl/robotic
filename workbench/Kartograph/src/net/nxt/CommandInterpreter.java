package net.nxt;

import net.nxt.commands.CommandException;
import net.packets.commands.CommandPacket;

/**
 * Interface to provide handling of commands.
 * @author loris.sauter
 *
 */
public interface CommandInterpreter {
	
	/**
	 * Interprets the given command packet.
	 * @param cmdPckt The command to interpret
	 * @return TRUE on success
	 */
	public boolean interpretCommand(CommandPacket cmdPckt) throws CommandException;
	

}
