package junit;

import static org.junit.Assert.*;
import lejos.geom.Point;

import org.junit.Test;

import control.MesureMap;

public class testControlMesureMap {

	@Test
	public void testConstructor() {
		MesureMap m=new MesureMap(5);
		if(m.mesured.length!=m.mesured[0].length){
			fail("Area isn't quadratic");
		}
		if(m.mesured.length!=5){
			fail("Wrong size for the Resolution 20cm");
		}
	}
	@Test
	public void testPredict(){
		MesureMap m=new MesureMap(3);
		m.mesured[0][0]=true;
		m.unreachabel[0][0]=false;
		m.mesured[0][1]=true;
		m.unreachabel[0][1]=false;
		m.mesured[0][2]=true;
		m.unreachabel[0][2]=false;
		m.mesured[1][0]=true;
		m.unreachabel[1][0]=false;
		m.mesured[1][1]=true;
		m.unreachabel[1][1]=false;
		m.mesured[1][2]=true;
		m.unreachabel[1][2]=false;
		m.mesured[2][0]=true;
		m.unreachabel[2][0]=false;
		m.mesured[2][1]=true;
		m.unreachabel[2][1]=false;
		m.mesured[2][2]=false;
		m.unreachabel[2][2]=false;
		System.out.println(m.getNearest2(new Point(0,0)));
		
		m.mesured[0][0]=false;
		m.unreachabel[0][0]=false;
		m.mesured[0][1]=true;
		m.unreachabel[0][1]=false;
		m.mesured[0][2]=true;
		m.unreachabel[0][2]=false;
		m.mesured[1][0]=true;
		m.unreachabel[1][0]=false;
		m.mesured[1][1]=true;
		m.unreachabel[1][1]=false;
		m.mesured[1][2]=true;
		m.unreachabel[1][2]=false;
		m.mesured[2][0]=true;
		m.unreachabel[2][0]=false;
		m.mesured[2][1]=true;
		m.unreachabel[2][1]=false;
		m.mesured[2][2]=true;
		m.unreachabel[2][2]=false;
		System.out.println(m.getNearest2(new Point(0,0)));
		
		m.mesured[0][0]=false;
		m.unreachabel[0][0]=false;
		m.mesured[0][1]=false;
		m.unreachabel[0][1]=false;
		m.mesured[0][2]=false;
		m.unreachabel[0][2]=false;
		m.mesured[1][0]=false;
		m.unreachabel[1][0]=false;
		m.mesured[1][1]=false;
		m.unreachabel[1][1]=false;
		m.mesured[1][2]=false;
		m.unreachabel[1][2]=false;
		m.mesured[2][0]=false;
		m.unreachabel[2][0]=false;
		m.mesured[2][1]=false;
		m.unreachabel[2][1]=false;
		m.mesured[2][2]=false;
		m.unreachabel[2][2]=false;
	}

}
