package logging;


/**
 * The {@link Logger} interface provides the logging methods.
 * 
 * The level handling must be implemented by the classes itself.
 * @author loris.sauter
 * @version 0.2
 */
public interface Logger extends Logging {
	
	/**
	 * Log on provided level.
	 * @param msg Message to log
	 */
	public void log(String msg);
	
	/**
	 * Log a message on DEBUG level
	 * @param msg Message to log
	 */
	public void debug(String msg);

	/**
	 * Log a message on INFO level
	 * @param msg Message to log
	 */
	public void info(String msg);
	
	/**
	 * Log a message on WARN level
	 * @param msg Message to log
	 */
	public void warn(String msg);
	
	/**
	 * Log a message on ERROR level
	 * @param msg Message to log
	 */
	public void error(String msg);
	
	/**
	 * Returns the level of this logger.
	 * @return the level of this logger-
	 */
	public byte getLevel();
}
