package net.packets.commands;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import net.packets.PacketIDRegistry;

public class OKCmdPacket implements CommandPacket {

	@Override
	public void write(DataOutputStream to) throws IOException {
		to.write(this.getId());
		to.flush();
	}

	@Override
	public void read(DataInputStream from) throws IOException {
		//only send packet id
	}

	@Override
	public int getId() {
		return PacketIDRegistry.CMD_OK;
	}

	@Override
	public void executeCommand() {
		//Does nothing
	}

}
