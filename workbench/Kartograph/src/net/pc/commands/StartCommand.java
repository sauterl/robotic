/**
 * 
 */
package net.pc.commands;

import java.io.IOException;

import net.packets.EndOfStreamException;
import net.packets.MesureMapDataPacket;
import net.packets.NotSetupException;
import net.packets.Packet;
import net.packets.PacketException;
import net.packets.commands.CmdStart;
import net.packets.commands.OKCmdPacket;
import control.MesureMap;

/**
 * @author loris.sauter
 *
 */
public class StartCommand extends AbstractCommand {

	private MesureMap map;
	
	/**
	 * @param pckt
	 */
	public StartCommand(MesureMap map) {
		super(new CmdStart() );
	}

	/** 
	 * {@inheritDoc}
	 * @see net.pc.commands.AbstractCommand#onReceive(net.packets.Packet)
	 */
	@Override
	public boolean onReceive(Packet answer) throws UnexpectedAnswerException {
		Packet out = answer;
		if(answer instanceof OKCmdPacket){
			try {
				handler.send(new MesureMapDataPacket(map));
			} catch (NotSetupException | IOException e) {
				e.printStackTrace();
				return false;
			}
			try {
				Packet rec = handler.getNextPacket();
				if(rec instanceof OKCmdPacket){
					handler.readPacket(rec);
					System.out.println("OK on nxt received mesmap");
					return true;
				}else{
					out = rec;
				}
			} catch (PacketException | IOException | NotSetupException
					| EndOfStreamException e) {
				System.out.println("Err on command internal receive (StartCommand)");
				e.printStackTrace();
				return false;
			}
			
		}
		throw new UnexpectedAnswerException(out);
	}

}
