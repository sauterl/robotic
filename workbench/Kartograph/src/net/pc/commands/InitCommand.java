/**
 * 
 */
package net.pc.commands;

import lejos.geom.Point;
import net.packets.Packet;
import net.packets.commands.CmdInitPacket;
import net.packets.commands.OKCmdPacket;

/**
 * @author loris.sauter
 *
 */
public class InitCommand extends AbstractCommand {
	
	public InitCommand(int size, Point start){
		super(new CmdInitPacket(size, start));
	}

	/** 
	 * {@inheritDoc}
	 * @see net.pc.commands.AbstractCommand#onReceive(net.packets.Packet)
	 */
	@Override
	public boolean onReceive(Packet answer) throws UnexpectedAnswerException {
		if(answer instanceof OKCmdPacket){
			return true;
		}
		throw new UnexpectedAnswerException(answer);
	}

}
