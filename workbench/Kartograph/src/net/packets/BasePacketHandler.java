/**
 * 
 */
package net.packets;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import net.packets.commands.CloseCmdPacket;
import net.packets.commands.CmdDrive;
import net.packets.commands.CmdInitPacket;
import net.packets.commands.CmdMap;
import net.packets.commands.CmdMesure;
import net.packets.commands.CmdNextPoint;
import net.packets.commands.CmdStart;
import net.packets.commands.ConnectCmdPacket;
import net.packets.commands.OKCmdPacket;
import net.packets.commands.ReqTestCmdPacket;
import vectormap.Line;

/**
 * The BasePacket handler provides the basic Packet Protocol based operations
 * like getting the next packet and sending one.
 * @author loris.sauter
 * @version 1.0
 */
public class BasePacketHandler {

	/**
	 * The internal DataInputStream to read on.
	 */
	protected DataInputStream dis = null;
	/**
	 * The internal DataOutputStream to write to
	 */
	protected DataOutputStream dos = null;
	
	/**
	 * Creates a unconnected {@link AbstractPacketHandler}.
	 */
	public BasePacketHandler(){
		
	}
	
	/**
	 * Checks whether this PacketHandler is set up correctly or not.
	 * It will ALWAYS return FALSE if the setup method got not called properly.
	 * @return TRUE if the DataInputStream and the DataOutputStream are not null - FALSE otherwise
	 */
	public boolean checkSetup(){
		if(dis != null & dos != null){
			return true;
		}else{
			return false;
		}
	}
	
	/**
	 * Sets this PacketHandler up.
	 * This method does not check if the streams are valid or not null
	 * @param dis The DataInputStream to read on
	 * @param dos The DataOutputStream to write to
	 */
	public void setup(DataInputStream dis, DataOutputStream dos){
		this.dis = dis;
		this.dos = dos;
	}
	
	/**
	 * Reads the next packet on the stream and returns it.
	 * @return The next packet on the stream.
	 * @throws IOException If an IO error occurs
	 * @throws PacketException If the given packet ID is not supported
	 * @throws NotSetupException If the streams are null.
	 */
	public Packet getNextPacket() throws IOException, PacketException, NotSetupException, EndOfStreamException{
		if(!checkSetup() ){
			throw new NotSetupException();
		}
		int flag = this.dis.read();
		switch(flag){
		case -1://End Of Stream
			throw new EndOfStreamException();
		/* === SUPPORTED RAW TYPES === */
		case PacketIDRegistry.STRING:
			return new StringDataPacket();
		case PacketIDRegistry.BOOLEAN:
			return new BooleanDataPacket();
		case PacketIDRegistry.BYTE:
			return new ByteDataPacket();
		case PacketIDRegistry.FLOAT:
			return new FloatDataPacket();
		case PacketIDRegistry.INTEGER:
			return new IntegerDataPacket();
		/* === SUPPORTED ADVANCED TYPES === */
		case PacketIDRegistry.LINE:
			return new Line();
		case PacketIDRegistry.POINT:
			return new PointDataPacket();
		case PacketIDRegistry.MESURE_MAP:
			return new MesureMapDataPacket();
		case PacketIDRegistry.VECTOR_MAP:
			return new VectorMapDataPacket();
		/* === COMMANDS === */
		case PacketIDRegistry.CMD_OK:
			return new OKCmdPacket();
		case PacketIDRegistry.CMD_CONNECT:
			return new ConnectCmdPacket();
		case PacketIDRegistry.CMD_CLOSE:
			return new CloseCmdPacket();
		case PacketIDRegistry.CMD_DRIVE:
			return new CmdDrive();
		case PacketIDRegistry.CMD_INIT:
			return new CmdInitPacket();
		case PacketIDRegistry.CMD_MAP:
			return new CmdMap();
		case PacketIDRegistry.CMD_MESURE:
			return new CmdMesure();
		case PacketIDRegistry.CMD_NEXTPOINT:
			return new CmdNextPoint();
		case PacketIDRegistry.CMD_START:
			return new CmdStart();
		case PacketIDRegistry.CMD_TEST:
			return new ReqTestCmdPacket();
		default:
			throw new PacketException("The given ID ("+flag+") is not supported.");
		}
	}
	
	/**
	 * Sends the given packet on the set DataOutputStream.
	 * @param packet The packet to send
	 * @throws NotSetupException If the DataOutputStream or the DataInputStream were null
	 * @throws IOException If an IO error occurs
	 */
	public void send(Packet packet) throws NotSetupException, IOException{
		if(!checkSetup() ){
			throw new NotSetupException();
		}
		packet.write(dos);
	}
	
	/**
	 * Closes the internal streams
	 */
	public void close(){
		if(!checkSetup() ){
			return;
		}
		try {
			this.dis.close();
		} catch (IOException e) {
			System.err.println("Error on closing dis");
			e.printStackTrace();
		} catch (NullPointerException e){
			System.err.println("Cannot close NULL-stream (dis)");
		}
		
		try {
			this.dos.close();
		} catch (IOException e) {
			System.err.println("Error on closing dos");
			e.printStackTrace();
		}catch(NullPointerException e){
			System.err.println("Cannot close NULL-stream (dos)");
		}
	}

	/**
	 * Reads the contents of the given packet.
	 * This just calls the packet's read method.
	 * @param p The packet to read
	 * @throws IOException If an IO error occurs
	 */
	public void readPacket(Packet p) throws IOException {
		p.read(dis);
	}

}
