package net.test;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;


import net.packets.PacketIDRegistry;
import net.packets.StringDataPacket;
import lejos.nxt.Button;
import lejos.nxt.comm.Bluetooth;
import lejos.nxt.comm.NXTConnection;

/**
 * NXT RECEIVER
 * 
 * A test class to quickly test code on the nxt brick.
 * This class is firstly establishes a bluetooth connection and then tests
 * some sendings
 * @author Loris
 *
 */
public class NXTTest {
	public static void main(String[] args) {
		System.out.println("Waiting...");
		NXTConnection conn = Bluetooth.waitForConnection();
		while (true) {
			if (conn != null) {
				DataInputStream dis = conn.openDataInputStream();
				DataOutputStream dos = conn.openDataOutputStream();

				// for(int i=0; i<100; i++){
				// try {
				// int read = dis.readInt();
				// System.out.println("read: "+read);
				// dos.writeInt(read);
				// dos.flush();
				// } catch (IOException e) {
				// System.out.println("Could not read!");
				// e.printStackTrace();
				// break;
				// }
				// }
				int counter = 0;
				boolean doRead = true;
				while (doRead) {
					/* PacketHandler & Line implements DataPacket test */
//					PacketHandler h = new PacketHandler(dis, dos);
//					boolean succ = h.readNextPacket();
//					if(!succ){
//						break;
//					}
//					/* StringDataPacket test */
//					try {
//						int flag = dis.read();
//						if (flag == PacketIDRegistry.STRING) {
//							StringDataPacket sdp = new StringDataPacket();
//							sdp.read(dis);
//							String in = sdp.get();
//							sdp.set(in + " World");
//							sdp.write(dos);
//						}
//					} catch (IOException ex) {
//						System.out.println("Ex occ");
//						ex.printStackTrace();
//						doRead = false;
//					}

					/*OLD TESTS*/
					// try {
					// int read = dis.readInt();
					// System.out.println("read: "+read);
					// dos.writeInt(++counter);
					// dos.flush();
					// } catch (IOException e) {
					// System.out.println("Could not read!");
					// e.printStackTrace();
					// doRead = false;
					// }
				}
				try {
					dis.close();
				} catch (IOException e) {
					System.out.println("failed closing in");
					e.printStackTrace();
				}
				try {
					dos.close();
				} catch (IOException e) {
					System.out.println("failed closing out");
					e.printStackTrace();
				}
				System.out.println("Closing");
				conn.close();
				System.out.println("Closed. Terminating");
				Button.waitForAnyPress();
				break;
			}
		}
	}
}
