/**
 * 
 */
package net.nxt.commands;

import java.io.IOException;

import control.MesureMap;
import lejos.geom.Point;
import net.packets.EndOfStreamException;
import net.packets.MesureMapDataPacket;
import net.packets.NotSetupException;
import net.packets.Packet;
import net.packets.PacketException;
import net.packets.PointDataPacket;
import net.packets.commands.CmdDrive;
import net.packets.commands.CmdNextPoint;
import net.packets.commands.OKCmdPacket;

/**
 * @author loris.sauter
 *
 */
public class NextPointSequenz extends CommandSequenz {

	/**
	 * 
	 */
	public NextPointSequenz() {
	}

	/** 
	 * {@inheritDoc}
	 * @see net.nxt.commands.CommandSequenz#executeSequenz(net.packets.Packet)
	 */
	@Override
	public boolean executeSequenz(Packet packet) throws SequenzException {
		if(packet instanceof CmdNextPoint){
			Point otherRobot = ((CmdNextPoint) packet).getOtherRobotPosition();
			System.out.println("OR: "+otherRobot.x+","+otherRobot.y);
			try {
				handler.send(new OKCmdPacket());
			} catch (NotSetupException | IOException e3) {
				System.out.println("ERR: sendok");
				e3.printStackTrace();
				return false;
			}
			Packet map;
			try {
				map = handler.getNextPacket();
				if(map instanceof MesureMapDataPacket){
					MesureMap mm = ((MesureMapDataPacket) map).get();
					Point next = interpreter.getController().getNextPoint(mm.mesured, mm.unreachabel, otherRobot);
					handler.send(new PointDataPacket(next));
				}
			} catch (PacketException | IOException | NotSetupException
					| EndOfStreamException e2) {
				System.out.println("ERR: no map (NBS)");
				e2.printStackTrace();
			}
			Packet drv;
			try {
				drv = handler.getNextPacket();
			} catch (PacketException | IOException | NotSetupException
					| EndOfStreamException e) {
				System.out.println("ERR: next pckt (NPS)");
				e.printStackTrace();
				return false;
			}
			if(drv instanceof CmdDrive){
				try {
					handler.readPacket(drv);
				} catch (IOException e1) {
					System.out.println("ERR: read pckt");
					e1.printStackTrace();
					return false;
				}
				Point or = ((CmdDrive) drv).getOtherRobot();
				Point to = ((CmdDrive) drv).getToGo();
				System.out.println("Togo: "+to.x+","+to.y);
				Point reached = interpreter.getController().drive(to, otherRobot);
				try {
					handler.send(new PointDataPacket(reached));
				} catch (NotSetupException | IOException e) {
					System.out.println("ERR. snd point");
					e.printStackTrace();
					return false;
				}
				return true;
			}
		}
		return false;
	}

}
