package net.nxt;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import lejos.nxt.comm.NXTConnection;
import lejos.nxt.remote.NXTComm;

public class InputReader{

	private final NXTConnection connection;
	private DataInputStream dis;
	private DataOutputStream dos;
	
	public InputReader(NXTConnection connection){
		this.connection = connection;
		this.dis = this.connection.openDataInputStream();
		this.dos = this.connection.openDataOutputStream();
	}

	public int readCommand() throws IOException{
		int read = -100;
		read = this.dis.read();
		return read;
	}
	
	
}
