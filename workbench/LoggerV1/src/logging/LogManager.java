package logging;

/**
 * This class is capable of choosing the correct {@link Logger} implementation for a given LogConfig.
 * It's also the factory of the loggers.
 * 
 * @author loris.sauter
 * @version 0.1
 *
 */
public class LogManager {
	
	/**
	 * The default {@link LogConfig} that only logs on level {@link LogConfig#WARN_LEVEL}
	 */
	public static final LogConfig DEFAULT_CONFIG = new LogConfig();
	/**
	 * The {@link LogConfig} for debugging: it requests logs on level {@link LogConfig#DEBUG_LEVEL}
	 */
	public static final LogConfig DEBUG_CONFIG = new LogConfig(LogConfig.DEBUG_LEVEL);
	
	/**
	 * Returns a default configured {@link Logger} with specified name that logs to the console.
	 * @param name The name of the logger.
	 * @return A default configured {@link Logger} that logs to the console.
	 * @see LogManager#DEFAULT_CONFIG
	 */
	public static Logger getConsoleLogger(String name){
		return getConsoleLogger(name, DEFAULT_CONFIG );
	}
	
	/**
	 * Returns a default configured {@link Logger}, named like the argument {@link Logging}, which logs to the console.
	 * @param toLog The {@link Logging} that describes the name.
	 * @return A default configured {@link Logger}.
	 * @see LogManager#DEFAULT_CONFIG
	 */
	public static Logger getConsoleLogger(Logging toLog){
		return getConsoleLogger(toLog.getName() );
	}
	
	/**
	 * Returns a configured {@link Logger} with given name which logs to the console.
	 * @param name The name of the logger.
	 * @param config The configuration of the logger - if <code>null</code>, then {@link LogManager#DEFAULT_CONFIG} is chosen.
	 * @return A configured {@link Logger} that logs to the console.
	 */
	public static Logger getConsoleLogger(String name, LogConfig config){
		return new ConsoleLogger(name, config != null ? config.getLevel() : DEFAULT_CONFIG.getLevel() );
	}
	
	/**
	 * Returns a console {@link Logger} configured based on the given configuration and given {@link Logging}.
	 * @param toLog The {@link Logging} that describes the name
	 * @param config The config for the logger.
	 * @return A console logger with that given configuration.
	 */
	public static Logger getConsoleLogger(Logging toLog, LogConfig config){
		return getConsoleLogger(toLog.getName(), config);
	}
	
}
