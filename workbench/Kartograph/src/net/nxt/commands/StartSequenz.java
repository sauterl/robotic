/**
 * 
 */
package net.nxt.commands;

import java.io.IOException;

import net.packets.EndOfStreamException;
import net.packets.MesureMapDataPacket;
import net.packets.NotSetupException;
import net.packets.Packet;
import net.packets.PacketException;
import net.packets.commands.OKCmdPacket;

/**
 * @author loris.sauter
 *
 */
public class StartSequenz extends CommandSequenz {


	/** 
	 * {@inheritDoc}
	 * @see net.nxt.commands.CommandSequenz#executeSequenz()
	 */
	@Override
	public boolean executeSequenz(Packet packet) throws SequenzException {
		try {
			this.handler.send(new OKCmdPacket());
			try {
				Packet mapPckt = this.handler.getNextPacket();
				if(mapPckt instanceof MesureMapDataPacket){
					handler.readPacket(mapPckt);
					System.out.println("Read map");
					this.handler.send(new OKCmdPacket() );
					return true;
				}else{
					throw new SequenzException("No map");
				}
			} catch (PacketException | EndOfStreamException e) {
				e.printStackTrace();
			}
		} catch (NotSetupException | IOException e) {
			e.printStackTrace();
		}
		return false;
	}

}
