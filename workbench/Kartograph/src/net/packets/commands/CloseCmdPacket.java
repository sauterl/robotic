package net.packets.commands;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import net.packets.PacketIDRegistry;

public class CloseCmdPacket implements CommandPacket{

	@Override
	public int getId() {
		return PacketIDRegistry.CMD_CLOSE;
	}

	@Override
	public void write(DataOutputStream to) throws IOException {
		to.write(this.getId() );
		to.flush();
	}

	@Override
	public void read(DataInputStream from) throws IOException {
		// nothing
		
	}

	@Override
	public void executeCommand() {
		// nothing
		
	}

}
