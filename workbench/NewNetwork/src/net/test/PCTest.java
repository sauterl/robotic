package net.test;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import lejos.pc.comm.NXTComm;
import lejos.pc.comm.NXTCommException;
import lejos.pc.comm.NXTCommFactory;
import lejos.pc.comm.NXTInfo;
import net.sendings.DataIDManager;
import net.sendings.StringDataPacket;

public class PCTest {
	public static void main(String[] args){
		NXTComm com = null;
		try {
			com = NXTCommFactory.createNXTComm(NXTCommFactory.BLUETOOTH);
		} catch (NXTCommException e) {
			e.printStackTrace();
			die();
		}
//		NXTInfo info = new NXTInfo(NXTCommFactory.BLUETOOTH, "NXT", "");
		NXTInfo[] infos = null;
		try {
			infos = com.search("NXT");
		} catch (NXTCommException e) {
			e.printStackTrace();
			die();
		}
		if(infos != null){
			for(NXTInfo nfo : infos){
				try {
					com.open(nfo);
					InputStream is = com.getInputStream();
					OutputStream os = com.getOutputStream();
					DataInputStream dis = new DataInputStream(is);
					DataOutputStream dos = new DataOutputStream(os);
//					for(int i=0;i<100;i++){
//						dos.writeInt(i);
//						dos.flush();
//						int read = dis.readInt();
//						System.out.println("Read: "+read);
//					}
					StringDataPacket strp = new StringDataPacket("Hello");
					strp.write(dos);
					int flag = dis.read();
					if(flag == DataIDManager.STRING){
						StringDataPacket in = new StringDataPacket();
						in.read(dis);
						System.out.println("Read: "+in.get());
					}else{
						System.out.println("Flag: "+flag+" did not expect that!");
					}
				} catch (NXTCommException | IOException e) {
					e.printStackTrace();
				}
				
			}
		}
	}
	
	
	public static void die(){
		System.err.println("Dying now!");
		System.exit(-1);
	}
	
	public static void die(String msg){
		System.err.println(msg);
		System.err.println("Dying now!");
		System.exit(-1);
	}
}
