package net.packets;

/**
 * This class simply provides the IDs for the different Packets.
 * 
 * To register a new DataPacket it MUST BE HARD CODED added into this file.
 * However, the maximal value the ID can have is 255.
 * 
 * In the DataPacket.getId method, return the corresponding value of this class!
 * 
 * @author Loris
 * 
 */
public final class PacketIDRegistry {

	private PacketIDRegistry() {
	}

	public static final int NONE = 0;
	public static final int BOOLEAN = 1;
	public static final int BYTE = BOOLEAN + 1;
//	 public static final int SHORT = BOOLEAN + 2;
	public static final int INTEGER = BOOLEAN + 3;
	public static final int FLOAT = BOOLEAN + 4;
//	 public static final int DOUBLE = BOOLEAN + 5;
//	 public static final int CHAR = BOOLEAN + 6;
	public static final int STRING = BOOLEAN + 7;

	public static final int OBJECT = 16;
	public static final int LINE = OBJECT + 1;
	public static final int POINT = OBJECT + 2;
	public static final int MESURE_MAP = OBJECT + 3;
	public static final int VECTOR_MAP = OBJECT + 4;

	public static final int CMD = 64;
	public static final int CMD_OK = CMD + 1;
	public static final int CMD_CONNECT = CMD + 2;
	public static final int CMD_CLOSE = CMD + 3;
	public static final int CMD_START = CMD + 4;
	public static final int CMD_MAP = CMD + 5;
	public static final int CMD_DRIVE = CMD + 6;
	public static final int CMD_MESURE = CMD + 7;
	public static final int CMD_NEXTPOINT = CMD + 8;
	public static final int CMD_INIT = CMD + 9;
	
	public static final int CMD_LAST = CMD + 10;
	public static final int NB_CMD = CMD_LAST - CMD + 1;
	
	/**
	 * Test ID
	 */
	public static final int CMD_TEST = CMD + CMD;

}
