package logging;

/**
 * The {@link Logging} interface provides a simple shortcut for the {@link LogManager} to get the name of a logger.
 * 
 * @author loris.sauter
 * @version 0.1
 */
public interface Logging {

	/**
	 * Returns the name of this {@link Logging}.
	 * @return The name of this {@link Logging}.
	 */
	public String getName();
}
