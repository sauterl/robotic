package net.packets.commands;

import net.packets.Packet;

/**
 * 
 * @author loris.sauter
 *
 */
public interface CommandPacket extends Packet {

	/**
	 * Experimental method, may get removed
	 */
	public void executeCommand();
}
