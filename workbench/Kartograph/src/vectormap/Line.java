package vectormap;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import lejos.geom.Point;
import net.packets.Packet;
import net.packets.PacketIDRegistry;

/**
 * this class represents a vector in a cartesian coordinate system and is designed to use little disk space.
 * 
 * @author Eddie
 *
 */
public class Line implements Packet {
	private float ex;
	private float ey;
	private float sx;
	private float sy;

	/**
	 * Creates a new Line, that is actually a point since start and end point are both 0,0.
	 */
	public Line() {
		ex = 0;
		ey = 0;
		sx = 0;
		sy = 0;
	}

	/**
	 * Default constructor of the {@code Line}. this constructor will turn the line to a positive x direction.
	 * 
	 * @param startX x-coordinate of the start
	 * @param startY y-coordinate of the start
	 * @param endX x-coordinate of the end
	 * @param endY y-coordinate of the end
	 */
	public Line(float startX, float startY, float endX, float endY) {
		if (endX > startX) {
			sx = startX;
			sy = startY;
			ex = endX;
			ey = endY;
		} else {
			ex = startX;
			ey = startY;
			sx = endX;
			sy = endY;
		}
	}

	public Line(Point start, Point end) {
		float startX = start.x;
		float startY = start.y;
		float endX = end.x;
		float endY = end.y;

		if (endX > startX) {
			sx = startX;
			sy = startY;
			ex = endX;
			ey = endY;
		} else {
			ex = startX;
			ey = startY;
			sx = endX;
			sy = endY;
		}
	}

	/**
	 * Get the direction of the {@code Line}
	 * 
	 * @return the direction in degrees in a cartesian coordinate system. Degrees only from 0 to 180.
	 */
	public float direction() {
		if (radDeg(Math.acos((ey - sy) / lenght())) != 180) {
			return radDeg(Math.acos((ey - sy) / lenght()));
		} else {
			return 0;
		}
	}

	public Point getEnd() {
		return new Point(ex, ey);
	}

	/**
	 * Get the x-coordinate of the end of the {@code Line}
	 * 
	 * @return x-coordinate in centimeters
	 */
	public float getEndX() {
		return ex;
	}

	/**
	 * Get the y-coordinate of the end of the {@code Line}
	 * 
	 * @return y-coordinate in centimeters
	 */
	public float getEndY() {
		return ey;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getId() {
		return PacketIDRegistry.LINE;
	}

	public Point getStart() {
		return new Point(sx, sy);
	}

	// public void overwrite(float startX, float startY, float endX, float endY) {
	// if (endX > startX) {
	// sx = startX;
	// sy = startY;
	// ex = endX;
	// ey = endY;
	// } else {
	// ex = startX;
	// ey = startY;
	// sx = endX;
	// sy = endY;
	// }
	// }

	// /**
	// * Set the x-coordinate for the end of the {@code Line}.
	// *
	// * @param endX x-coordinate in centimeters
	// * @deprecated since the constructor turns the line to point to a positive x direction. This could possibly
	// produce
	// * incorrect output of other functions.
	// */
	// @Deprecated
	// public void setEndX(float endX) {
	// this.ex = endX;
	// }

	// /**
	// * Set the y-coordinate for the end of the {@code Line}.
	// *
	// * @param endY y-coordinate in centimeters
	// * @deprecated since the constructor turns the line to point to a positive x direction. This could possibly
	// produce
	// * incorrect output of other functions.
	// */
	// @Deprecated
	// public void setEndY(float endY) {
	// this.ey = endY;
	// }

	// /**
	// * Set the x-coordinate for the start of the {@code Line}.
	// *
	// * @param startX x-coordinate in centimeters
	// * @deprecated since the constructor turns the line to point to a positive x direction. This could possibly
	// produce
	// * incorrect output of other functions.
	// */
	// @Deprecated
	// public void setStartX(float startX) {
	// this.sx = startX;
	// }

	// /**
	// * Set the y-coordinate for the start of the {@code Line}.
	// *
	// * @param startY y-coordinate in centimeters
	// * @deprecated since the constructor turns the line to point to a positive x direction. This could possibly
	// produce
	// * incorrect output of other functions.
	// */
	// @Deprecated
	// public void setStartY(float startY) {
	// this.sy = startY;
	// }

	/**
	 * Get the x-coordinate of the start of the {@code Line}
	 * 
	 * @return x-coordinate in centimeters
	 */
	public float getStartX() {
		return sx;
	}

	/**
	 * Get the y-coordinate of the start of the {@code Line}
	 * 
	 * @return y-coordinate in centimeters
	 */
	public float getStartY() {
		return sy;
	}

	/**
	 * Get the length of the {@code Line}
	 * 
	 * @return the length in centimeters
	 */
	public float lenght() {
		return (float) Math.sqrt(Math.pow(ex - sx, 2) + Math.pow(ey - sy, 2));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void read(DataInputStream from) throws IOException {
		this.ex = from.readFloat();
		this.ey = from.readFloat();
		this.sx = from.readFloat();
		this.sy = from.readFloat();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void write(DataOutputStream to) throws IOException {
		to.write(this.getId());
		to.writeFloat(ex);
		to.writeFloat(ey);
		to.writeFloat(sx);
		to.writeFloat(sy);
		to.flush();
	}

	/**
	 * Method to convert from radiant to degrees
	 * 
	 * @param rad angle in radiant
	 * @return angle in degrees
	 */
	private float radDeg(double rad) {
		return (float) (rad / Math.PI * 180);
	}
}
