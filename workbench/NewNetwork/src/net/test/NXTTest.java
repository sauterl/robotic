package net.test;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import net.sendings.DataIDManager;
import net.sendings.StringDataPacket;
import lejos.nxt.comm.Bluetooth;
import lejos.nxt.comm.NXTConnection;

public class NXTTest {
	public static void main(String[] args) {
		System.out.println("Waiting...");
		NXTConnection conn = Bluetooth.waitForConnection();
		while (true) {
			if (conn != null) {
				DataInputStream dis = conn.openDataInputStream();
				DataOutputStream dos = conn.openDataOutputStream();

				// for(int i=0; i<100; i++){
				// try {
				// int read = dis.readInt();
				// System.out.println("read: "+read);
				// dos.writeInt(read);
				// dos.flush();
				// } catch (IOException e) {
				// System.out.println("Could not read!");
				// e.printStackTrace();
				// break;
				// }
				// }
				int counter = 0;
				boolean doRead = true;
				while (doRead) {
					try {
						int flag = dis.read();
						if (flag == DataIDManager.STRING) {
							StringDataPacket sdp = new StringDataPacket();
							sdp.read(dis);
							String in = sdp.get();
							sdp.set(in + " World");
							sdp.write(dos);
						}
					} catch (IOException ex) {
						System.out.println("Ex occ");
						ex.printStackTrace();
						doRead = false;
					}

					// try {
					// int read = dis.readInt();
					// System.out.println("read: "+read);
					// dos.writeInt(++counter);
					// dos.flush();
					// } catch (IOException e) {
					// System.out.println("Could not read!");
					// e.printStackTrace();
					// doRead = false;
					// }
				}
				try {
					dis.close();
				} catch (IOException e) {
					System.out.println("failed closing in");
					e.printStackTrace();
				}
				try {
					dos.close();
				} catch (IOException e) {
					System.out.println("failed closing out");
					e.printStackTrace();
				}
				conn.close();
			}
		}
	}
}
