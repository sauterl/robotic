/**
 * 
 */
package net.nxt;

import net.nxt.commands.SequenzException;

/**
 * Exception to indicate that the connection got closed
 * @author loris.sauter
 *
 */
public class ClosedConnectionException extends SequenzException {

	/**
	 * defautl const
	 */
	public ClosedConnectionException() {
		super("Master closed conn");
	}

	/**
	 * @param message
	 */
	public ClosedConnectionException(String message) {
		super(message);
	}

}
