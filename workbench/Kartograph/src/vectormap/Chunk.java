package vectormap;

import java.io.IOException;

import lejos.geom.Point;
import lejos.geom.Rectangle;
import lejos.robotics.mapping.LineMap;
import lejos.robotics.navigation.DestinationUnreachableException;
import lejos.robotics.navigation.Pose;
import lejos.robotics.navigation.Waypoint;
import lejos.robotics.pathfinding.Path;
import logic.FileWriter;
import control.Location;
import control.MesureMap;

public class Chunk {
//	private static final int DEGREE_TOLLERANCE = 15;
//
//	private static final float GAPP_TOLLERANCE = 10f;
//	private static final float GROWTH_MIN = 0.2f;
//	private static final float LENGHT_DIF = 0.2f;
	
//	private static final int DEGREE_TOLLERANCE = 25;
//	private static final float GAPP_TOLLERANCE = 15f;
//	private static final float GROWTH_MIN = 0.4f;
//	private static final float LENGHT_DIF = 0.4f;
	
	private Object[] groups;

	// tested with data and had the best merging result till now. Merger 140 lines to 18 lines with only a littel
	// mistake.
	 private static final int DEGREE_TOLLERANCE = 20;
	 private static final float GAPP_TOLLERANCE = 35f;
	 private static final float GROWTH_MIN = 0.4f;
	 private static final float LENGHT_DIF = 0.4f;
	private MesureMap mesMap;
	private final byte MINBOX = 10;
	private lejos.robotics.pathfinding.ShortestPathFinder pf;
	private int size;

	// private int counter=0;

	private Point startPosition;

	public Chunk(int size, MesureMap mesMap) {
		groups = new Object[180];
		for (int i = 0; i < 180; i++) {
			groups[i] = new TinyList<Line>((byte) 4);
		}
		this.mesMap = mesMap;
		pf = new lejos.robotics.pathfinding.ShortestPathFinder(new LineMap(new lejos.geom.Line[0], new Rectangle(-1,
				-1, 2, 2)));
		this.size = size;
		startPosition = new Point(0, 0);
	}

	public Chunk(int size, MesureMap mesMap, Point startPosition) {
		groups = new Object[180];
		for (int i = 0; i < 180; i++) {
			groups[i] = new TinyList<Line>((byte) 4);
		}
		this.mesMap = mesMap;
		pf = new lejos.robotics.pathfinding.ShortestPathFinder(new LineMap(new lejos.geom.Line[0], new Rectangle(-1,
				-1, 2, 2)));
		this.size = size;
		this.startPosition = startPosition;
	}

	@SuppressWarnings("unchecked")
	public void addLine(Line l) {
		l = merge(l);
		Object t = groups[(int) l.direction()];
		((TinyList<Line>) t).add(l);

	}

	@SuppressWarnings("unchecked")
	public void exchangeLines(Line[] lines) {
		groups = new Object[180];
		for (int i = 0; i < 180; i++) {
			groups[i] = new TinyList<Line>((byte) 4);
		}
		for (Line l : lines) {
			Object t = groups[(int) l.direction()];
			((TinyList<Line>) t).add(l);
		}
	}

	// public Point[] findIntersect(Line l) {
	//
	// float xs = l.getEndX() - l.getStartX();
	// float ys = l.getEndY() - l.getStartY();
	// float x = l.getStartX();
	// float y = l.getStartY();
	// float tmp = x / xs;
	// float b = y + tmp * ys;
	// float a = 1 / xs;
	// Point[] intersects = null;
	// for (Line m : getLines()) {
	// xs = m.getEndX() - m.getStartX();
	// ys = m.getEndY() - m.getStartY();
	// x = m.getStartX();
	// y = m.getStartY();
	// tmp = x / xs;
	// float d = y + tmp * ys;
	// float c = 1 / xs;
	//
	// float intersect = (d - b) / (a - c);
	// if (intersect == Float.NaN) {
	// if (a * m.getStartX() + b - m.getStartY() < 0.3) {
	// enlargen(intersects, m.getStart());
	// }
	// continue;
	// }
	// enlargen(intersects, new Point(intersect, a * intersect + b));
	//
	// }
	// return intersects;
	// }

	public Path findWay(Point loc, Point end, Point r2) throws DestinationUnreachableException {
		Line[] lch = this.getLines();
		lejos.geom.Line[] lines;
		if (r2 != null) {
			lines = new lejos.geom.Line[lch.length + 4];

			lines[lch.length] = new lejos.geom.Line(r2.x - MINBOX, r2.y - MINBOX, r2.x + MINBOX, r2.y - MINBOX);
			lines[lch.length + 1] = new lejos.geom.Line(r2.x - MINBOX, r2.y + MINBOX, r2.x + MINBOX, r2.x + MINBOX);
			lines[lch.length + 2] = new lejos.geom.Line(r2.x - MINBOX, r2.y - MINBOX, r2.x - MINBOX, r2.y + MINBOX);
			lines[lch.length + 3] = new lejos.geom.Line(r2.x + MINBOX, r2.y - MINBOX, r2.x + MINBOX, r2.y + MINBOX);
		} else {
			lines = new lejos.geom.Line[lch.length];
		}

		for (int i = 0; i < lch.length; i++) {
			lines[i] = new lejos.geom.Line(lch[i].getStartX(), lch[i].getStartY(), lch[i].getEndX(), lch[i].getStartY());
		}

		lejos.robotics.mapping.LineMap lMap = new lejos.robotics.mapping.LineMap(lines, new Rectangle(mesMap.getSize()
				/ -2, mesMap.getSize() / -2, mesMap.getSize(), mesMap.getSize()));
		// System.out.println("Before");
		// lejos.robotics.pathfinding.ShortestPathFinder pf=new lejos.robotics.pathfinding.ShortestPathFinder(lMap);
		lMap.flip();
		pf.setMap(lMap);
		pf.lengthenLines(10);
		// return pf.findRoute(new Pose((float)loc.getX(), (float)loc.getY()*-1, loc.getDirection()), new
		// Waypoint(end.getX(),end.getY()*-1));
		return pf.findRoute(new Pose((float) loc.getX(), (float) loc.getY(), 0), new Waypoint(end));
	}

	// public String getData() {
	// String ret = "x1\ty1\ty2\tx2\n";
	// for (Line l : getLines()) {
	// ret += (l.getStartX()) + "\t" + (l.getStartY()) + "\t" + (l.getEndX()) + "\t" + (l.getEndY());
	// ret += "\n";
	// }
	// return ret;
	// }

	@SuppressWarnings("unchecked")
	public Line[] getLines() {
		Line[] lines = new Line[getSize()];
		int c = 0;
		for (int i = 0; i < 180; i++) {
			for (int j = 0; j < ((TinyList<Line>) groups[i]).size(); j++) {
				lines[c++] = ((TinyList<Line>) groups[i]).getEntry((short) j);
			}
		}
		return lines;
	}

	public MesureMap getMesureMap() {
		return mesMap;
	}

	@SuppressWarnings("unchecked")
	public int getSize() {
		int count = 0;
		for (int i = 0; i < 180; i++) {
			count += ((TinyList<Line>) groups[i]).size();
		}
		return count;
	}

	// public String getSVG() {
	// String ret = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" + "<svg xmlns=\"http://www.w3.org/2000/svg\"\n"
	// + "version=\"1.1\" baseProfile=\"full\"\n" + "width=\"" + size + "px\" height=\"" + size
	// + "px\" viewBox=\"0 0 " + size + " " + size + "\">\n";
	// ret += "<line x1=\"0\" y1=\"" + size / 2 + "\" x2=\"" + size + "\" y2=\"" + size / 2
	// + "\" stroke=\"black\" stroke-width=\"1px\"/>\n";
	// ret += "<line x1=\"" + size / 2 + "\" y1=\"0\" x2=\"" + size / 2 + "\" y2=\"" + size
	// + "\" stroke=\"black\" stroke-width=\"1px\"/>\n";
	// for (Line l : getLines()) {
	// ret += "<line x1=\"" + (l.getStartY() + (size / 2)) + "\" y1=\"" + ((size / 2) - l.getStartX())
	// + "\" x2=\"" + (l.getEndY() + (size / 2)) + "\" y2=\"" + ((size / 2) - l.getEndX())
	// + "\" stroke=\"black\" stroke-width=\"1px\"/>";
	// ret += "\n";
	// }
	// ret += "</svg>";
	// return ret;
	// }

	public int normalize(int index) {
		if (index < 0) {
			return 180 + index;
		} else if (index > 179) {
			return index - 180;
		}
		return index;
	}

	public void setMesureMap(MesureMap m) {
		mesMap = m;
	}

	public void writeData(FileWriter w) {
		try {
			w.write("x1\ty1\ty2\tx2\n");
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		for (Line l : getLines()) {
			String ret = "";
			ret += (l.getStartX()) + "\t" + (l.getStartY()) + "\t" + (l.getEndX()) + "\t" + (l.getEndY());
			ret += "\n";
			try {
				w.write(ret);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		try {
			w.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// private Line combine2(Line f, Line s) {
	// Double[] len = new Double[4];
	// len[0] = getLen(f.getStartX(), f.getStartY(), s.getStartX(), s.getStartY());
	// len[1] = getLen(f.getStartX(), f.getStartY(), s.getEndX(), s.getEndY());
	// len[2] = getLen(f.getEndX(), f.getEndY(), s.getStartX(), s.getStartY());
	// len[3] = getLen(f.getEndX(), f.getEndY(), s.getEndX(), s.getEndY());
	// double max = 0;
	// for (int i = 0; i < 4; i++) {
	// if (len[i] > max) {
	// max = len[i];
	// }
	// }
	// // System.out.println(max+"   "+len[0]);
	// if (f.lenght() > s.lenght()) {
	// if (f.lenght() > max) {
	// return f;
	// }
	// } else {
	// if (s.lenght() > max) {
	// return s;
	// }
	// }
	// if (len[0] == max) {
	// // System.out.println(f.getStartX()+"   "+f.getStartY()+"   "+s.getStartX()+"   "+s.getStartY());
	// return new Line(f.getStartX(), f.getStartY(), s.getStartX(), s.getStartY());
	// } else if (len[1] == max) {
	// return new Line(f.getStartX(), f.getStartY(), s.getEndX(), s.getEndY());
	// } else if (len[2] == max) {
	// return new Line(f.getEndX(), f.getEndY(), s.getStartX(), s.getStartY());
	// } else if (len[3] == max) {
	// return new Line(f.getEndX(), f.getEndY(), s.getEndX(), s.getEndY());
	// }
	// return f;
	// }

	public void writeSVG(FileWriter w) {
		String ret = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" + "<svg xmlns=\"http://www.w3.org/2000/svg\"\n"
				+ "version=\"1.1\" baseProfile=\"full\"\n" + "width=\"" + size + "px\" height=\"" + size
				+ "px\" viewBox=\"0 0 " + size + " " + size + "\">\n";
		ret += "<line x1=\"0\" y1=\"" + size / 2 + "\" x2=\"" + size + "\" y2=\"" + size / 2
				+ "\" stroke=\"black\" stroke-width=\"1px\"/>\n";
		ret += "<line x1=\"" + size / 2 + "\" y1=\"0\" x2=\"" + size / 2 + "\" y2=\"" + size
				+ "\" stroke=\"black\" stroke-width=\"1px\"/>\n";
		try {
			w.write(ret);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		for (Line l : getLines()) {
			ret = "";
			ret += "<line x1=\"" + (l.getStartY() + (size / 2)) + "\" y1=\"" + ((size / 2) - l.getStartX())
					+ "\" x2=\"" + (l.getEndY() + (size / 2)) + "\" y2=\"" + ((size / 2) - l.getEndX())
					+ "\" stroke=\"black\" stroke-width=\"1px\"/>";
			ret += "\n";
			try {
				w.write(ret);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		try {
			w.write("</svg>");
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			w.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private Line combine(Line f, Line s) {
		Double[] len = new Double[4];
		len[0] = getLen(f.getStartX(), f.getStartY(), s.getStartX(), s.getStartY());
		len[1] = getLen(f.getStartX(), f.getStartY(), s.getEndX(), s.getEndY());
		len[2] = getLen(f.getEndX(), f.getEndY(), s.getStartX(), s.getStartY());
		len[3] = getLen(f.getEndX(), f.getEndY(), s.getEndX(), s.getEndY());
		double min = Double.MAX_VALUE;
		for (int i = 0; i < 4; i++) {
			if (len[i] < min) {
				min = len[i];
			}
		}
		double x1, y1, x2, y2, x3, y3, x4, y4;
		if (len[0] == min) {
			x1 = f.getEndX();
			y1 = f.getEndY();
			x2 = f.getStartX();
			y2 = f.getStartY();
			x3 = s.getStartX();
			y3 = s.getStartY();
			x4 = s.getEndX();
			y4 = s.getEndY();
		} else if (len[1] == min) {
			x1 = f.getEndX();
			y1 = f.getEndY();
			x2 = f.getStartX();
			y2 = f.getStartY();
			x3 = s.getEndX();
			y3 = s.getEndY();
			x4 = s.getStartX();
			y4 = s.getStartY();
		} else if (len[2] == min) {
			x1 = f.getStartX();
			y1 = f.getStartY();
			x2 = f.getEndX();
			y2 = f.getEndY();
			x3 = s.getStartX();
			y3 = s.getStartY();
			x4 = s.getEndX();
			y4 = s.getEndY();
		} else /* if(len[3]==min) */{
			x1 = f.getStartX();
			y1 = f.getStartY();
			x2 = f.getEndX();
			y2 = f.getEndY();
			x3 = s.getEndX();
			y3 = s.getEndY();
			x4 = s.getStartX();
			y4 = s.getStartY();
		}
		double maxLenght = max(s.lenght(), f.lenght());
		double minLenght = min(s.lenght(), f.lenght());
		if (getLen(x1, y1, x4, y4) > (maxLenght + minLenght * GROWTH_MIN)) {
			return new Line((float) x1, (float) y1, (float) x4, (float) y4);
		} else if (Math.abs(s.lenght() - f.lenght()) < maxLenght * LENGHT_DIF) {
			return new Line((float) (x1 + x4) / 2, (float) (y1 + y4) / 2, (float) (x2 + x3) / 2, (float) (y2 + y3) / 2);
		} else if (s.lenght() > f.lenght()) {
			return s;
		} else {
			return f;
		}
	}

	// private Point[] enlargen(Point[] act, Point p) {
	// if (act == null) {
	// act = new Point[1];
	// act[0] = p;
	// return act;
	// } else {
	// Point[] actt = new Point[act.length + 1];
	// for (int i = 0; i < act.length; i++) {
	// actt[i] = act[i];
	// }
	// actt[act.length] = p;
	// return actt;
	//
	// }
	// }

	private double getLen(double x1, double y1, double x2, double y2) {
		return Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2));
	}

	private double max(double a, double b) {
		if (a > b) {
			return a;
		} else {
			return b;
		}
	}

	@SuppressWarnings("unchecked")
	private Line merge(Line l) {
		int dir = (int) l.direction();
		for (int it = -DEGREE_TOLLERANCE; it <= DEGREE_TOLLERANCE; it++) {
			int i = normalize(dir + it);
			for (int j = 0; j < ((TinyList<Line>) groups[i]).size(); j++) {
				// System.out.println(counter++);
				if (near(l, ((TinyList<Line>) groups[i]).getEntry((short) j))) {
					l = combine(((TinyList<Line>) groups[i]).getEntry((short) j), l);
					((TinyList<Line>) groups[i]).remove((short) j);
					return merge(l);
				}
			}
		}
		return l;
	}

	private double min(double a, double b) {
		if (a < b) {
			return a;
		} else {
			return b;
		}
	}

	private boolean near(Line l, Line m) {
		if (getLen(0, 0, l.getStartX() - m.getStartX(), l.getStartY() - m.getStartY()) < GAPP_TOLLERANCE
				|| getLen(0, 0, l.getStartX() - m.getEndX(), l.getStartY() - m.getEndY()) < GAPP_TOLLERANCE
				|| getLen(0, 0, l.getEndX() - m.getStartX(), l.getEndY() - m.getStartY()) < GAPP_TOLLERANCE
				|| getLen(0, 0, l.getEndX() - m.getEndX(), l.getEndY() - m.getEndY()) < GAPP_TOLLERANCE) {
			return true;
		}
		// System.out.println(getLen(0,0,l.getEndX()-m.getEndX(),l.getEndY()-m.getEndY()));
		// System.out.println((l.getEndX()-m.getEndX())+"     "+(l.getEndY()-m.getEndY()));
		return false;
	}

	// private boolean near2(Line l, Line m) {
	// float xs = l.getEndX() - l.getStartX();
	// float ys = l.getEndY() - l.getStartY();
	// float x = l.getStartX();
	// float y = l.getStartY();
	// float tmp = x / xs;
	// float b = y + tmp * ys;
	// float a = 1 / xs;
	//
	// xs = m.getEndX() - m.getStartX();
	// ys = m.getEndY() - m.getStartY();
	// x = m.getStartX();
	// y = m.getStartY();
	// tmp = x / xs;
	// float d = y + tmp * ys;
	// float c = 1 / xs;
	//
	// float intersect = (d - b) / (a - c);
	// // if(intersect==Float.NaN){
	// // getLen(0,0,)
	// // }
	// System.out.println(intersect);
	// if (intersect > l.getStartX() - GAPP_TOLLERANCE) {
	// if (intersect < l.getEndX() + GAPP_TOLLERANCE) {
	// if (intersect > m.getStartX() - GAPP_TOLLERANCE) {
	// if (intersect < m.getEndX() + GAPP_TOLLERANCE) {
	// return true;
	// }
	// }
	// }
	// }
	//
	// return false;
	// }

}
