package net.packets;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class ByteDataPacket implements DataPacket<Byte>{

	private byte b;
	
	public ByteDataPacket(){}
	
	public ByteDataPacket(byte b){
		this.b = b;
	}
	
	@Override
	public int getId() {
		return PacketIDRegistry.BYTE;
	}

	@Override
	public Byte get() {
		return b;
	}

	@Override
	public void set(Byte e) {
		this.b = e;
	}

	@Override
	public void write(DataOutputStream to) throws IOException {
		to.write(this.getId() );
		to.writeByte(b);
		to.flush();
	}

	@Override
	public void read(DataInputStream from) throws IOException {
		this.b = from.readByte();
	}

}
