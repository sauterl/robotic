package date;


public class DayInYear {
	private int day;
	private int month;

	public DayInYear(int day, int month) {
		this.day = day;
		this.month = month;
	}

	public int getDay() {
		return day;
	}

	@Override
	public String toString() {
		return "DayInYear [day=" + day + ", month=" + month + "]";
	}

	public void setDay(int day) {
		this.day = day;
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}
	
	public static DayInYear getDayInYear(int dayInYear, int year){
		if(year % 4 == 0){
			return getConversionTable(true)[dayInYear];
		}else{
			return getConversionTable(false)[dayInYear];
		}
	}
	
	private static DayInYear[] getConversionTable(boolean leap) {
		// conversion by http://disc.gsfc.nasa.gov/julian_calendar.shtml
		int days = leap ? 366 : 365;
		DayInYear[] out = new DayInYear[days];
		for (int m = 0; m < 12; m++) {
			int max = getMaxDayInMonth(m, leap);
			for(int d=1;d<=max;d++){
				out[m+d] = new DayInYear(d, m);
			}
		}
		return out;
	}
	
	private static int getMaxDayInMonth(int month, boolean leap){
		if(month < 0 || month > 11){
			throw new IllegalArgumentException("A month must be in range [0, 11], but was: "+month);
		}
		switch(month){
		case 0:case 2:case 4:case 6: case 7: case 9: case 11:
			return 31;
		case 1:
			return leap ? 29 : 28;
		default:
			return 30;
		}
	}

}
