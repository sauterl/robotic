/**
 * 
 */
package net.pc.commands;

import vectormap.Line;
import net.packets.Packet;
import net.packets.VectorMapDataPacket;
import net.packets.commands.CmdMap;

/**
 * @author loris.sauter
 *
 */
public class SendMapCommand extends AbstractCommand {
	
	private Line[] map;

	/**
	 * @param pckt
	 */
	public SendMapCommand() {
		super(new CmdMap() );
	}
	
	public Line[] getMap(){
		return map;
	}

	/** 
	 * {@inheritDoc}
	 * @see net.pc.commands.AbstractCommand#onReceive(net.packets.Packet)
	 */
	@Override
	public boolean onReceive(Packet answer) throws UnexpectedAnswerException {
		if(answer instanceof VectorMapDataPacket){
			map = ((VectorMapDataPacket) answer).get();
			return true;
		}
		return false;
	}

}
