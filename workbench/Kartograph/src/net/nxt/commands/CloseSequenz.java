/**
 * 
 */
package net.nxt.commands;

import net.nxt.ClosedConnectionException;
import net.packets.Packet;

/**
 * @author loris.sauter
 *
 */
public class CloseSequenz extends CommandSequenz {

	/** 
	 * {@inheritDoc}
	 * @see net.nxt.commands.CommandSequenz#executeSequenz()
	 */
	@Override
	public boolean executeSequenz(Packet packet) throws SequenzException{
		System.out.println("Closing due Master");
		throw new ClosedConnectionException();
	}

}
