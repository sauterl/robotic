package mesure;

public class MesureException extends Exception {
	public MesureException(String msg) {
		super(msg);
	}
}
