package control;

import lejos.nxt.Motor;
import lejos.robotics.navigation.Waypoint;
import lejos.robotics.pathfinding.Path;
import mesure.Mesure;

public class RobotControl {
	// private static final double BRAKEDEGREES = 10;
	/**
	 * Wheel diameter
	 */
	private static final double DIAMETER = 5.6;
	/**
	 * The tolerance left when turning the robot.
	 */
	private static final float TURN_TOLLERANCE = 0.2f;
	/**
	 * The location object that tracks the location of the robot.
	 */
	private Location loc;

	private Mesure mes;

	// /**
	// * default constructor
	// */
	// public RobotControl() {
	// loc = new Location(0, 0);
	// }

	public RobotControl(Mesure mes) {
		loc = new Location(0, 0);
		this.mes = mes;
	}

	/**
	 * This method calibrates the compass. this must only be done from time to time.
	 */
	public void calibrateCompass() {
		loc.startCal();
		Motor.B.setSpeed(180);
		Motor.C.setSpeed(180);
		Motor.B.setAcceleration(360);
		Motor.C.setAcceleration(360);
		Motor.C.rotate(720 * 3, true);
		Motor.B.rotate(-720 * 3);
		loc.stopCal();
	}

	/**
	 * drives the robot forward in a straight line.
	 * 
	 * @param distance distance the Robot shal drive in centimeters.
	 */
	public void drive(double distance) {
		drive(distance, false);
		// Setting.driveSettings(2000,200);
		// double degrees =-360*distance/(DIAMETER*Math.PI);
		// // Motor.B.resetTachoCount();
		// Motor.B.rotate((int)degrees,true);
		// Motor.C.rotate((int)degrees,false);
		// loc.move(distance);
	}

	/**
	 * drives the robot forward in a straight line.
	 * 
	 * @param distance distance the Robot shal drive in centimeters.
	 * @param ImieiateReturn if {@code true} returns emidiately from the method leaving the robot finish the driving
	 *            while calculating on.
	 */
	public void drive(double distance, boolean ImieiateReturn) {
		Setting.driveSettings(2000, 200);
		double degrees = -360 * distance / (DIAMETER * Math.PI);
		Motor.B.rotate((int) degrees, true);
		Motor.C.rotate((int) degrees, ImieiateReturn);
		// loc.move(distance);
		// System.out.println(Math.round(loc.getX())+"   "+Math.round(loc.getY()));
	}

	public void drive(Path p) throws RobotAboutToCrashException {
		Waypoint[] wps;
		wps = p.toArray(new Waypoint[p.size()]);
		for (Waypoint w : wps) {
			drive(w);
		}
	}

	public void drive(Waypoint w) throws RobotAboutToCrashException {
		// if(loc.getPosition().distance(w)>50){
		double dist = loc.getPosition().distance(w);
		// System.out.println(dist);
		float dir = loc.getAngle(w.x - loc.getX(), w.y - loc.getY());
		double dx = (w.x - loc.getX()) / dist;
		double dy = (w.y - loc.getY()) / dist;
		// System.out.println(dir);
		while (dist > 0) {
			// if(w.x-loc.getX()>0){
			turnToDegree(dir);
			// }else{
			// turnToDegree(dir+180);
			// }

			if ((mes.mesureFront() < 65 && dist > 50) || (mes.mesureFront() < dist + 15 && dist <= 50)) {
				throw new RobotAboutToCrashException();
			} else {
				if (dist > 500) {
					drive(50);
					loc.setX(loc.getX() + dx * 50);
					loc.setY(loc.getY() + dy * 50);
					dist -= 50;
				} else {
					drive(dist);
					loc.setX(loc.getX() + dx * dist);
					loc.setY(loc.getY() + dy * dist);
					dist = 0;
				}
				// loc.setX(w.x);
				// loc.setY(w.y);
				// }
			}
		}

	}

	// /**
	// * Drive the robot to a given coordinat, whereby the positive x-direction stands for north and the negative
	// * y-direction for east.
	// *
	// * @param x x coordinate in centimeters
	// * @param y y coordinate in centimeters
	// */
	// public void driveTo(double x, double y) {
	// // System.out.println(loc.getRelDistance(x,y));
	// //
	// System.out.println((int)loc.getX()+" "+(int)loc.getY()+" "+(int)loc.getDirection()+" "+loc.getRelAngle(loc.getX()-x,
	// // loc.getY()-y));
	// turnToDegree(loc.getAngle(x - loc.getX(), y - loc.getY()));
	//
	// drive((int) loc.getRelDistance(x, y));
	// loc.setX(x);
	// loc.setY(y);
	//
	// }

	public Location getLocation() {
		return loc;
	}

	// public Mesure getMes() {
	// return mes;
	// }

	public void setMes(Mesure mes) {
		this.mes = mes;
	}

	/**
	 * turns the robot counterclockwise.
	 * 
	 * @param angle Angle the robot shall be turned by in Degrees.
	 */
	public void turn(float angle) {
		turn(angle, false);

		// Setting.turnSettings();
		// int tangle=(int)(720.0*angle/-360.0);
		// Motor.B.rotate(-tangle,true);
		// Motor.C.rotate(tangle);
	}

	/**
	 * turns the robot counterclockwise.
	 * 
	 * @param angle Angle the robot shall be turned by in Degrees.
	 * @param ImieiateReturn if {@code true} returns emidiately from the method leaving the robot finish the turning
	 *            while calculating on.
	 */
	public void turn(float angle, boolean ImieiateReturn) {
		Setting.turnSettings();
		int tangle = (int) (720.0 * angle / -360.0);
		Motor.B.rotate(-tangle, true);
		Motor.C.rotate(tangle, ImieiateReturn);
	}

	/**
	 * Turns the robot to a given direction in the coordinatesystem.
	 * 
	 * @param angle The angle the robot shal be turned to in degrees.
	 */
	public void turnToDegree(float angle) {
		// System.out.println(loc.getDirection() + "  " + ((int) loc.getRelAngle(angle)) + "  " + angle);
		for (int count = 0; count < 5; count++) {
			if (Math.abs(angle - loc.getDirection()) > TURN_TOLLERANCE) {
				turn((int) loc.getRelAngle(angle));
				// System.out.print((int)loc.getRelAngle(angle));
			}
		}

		// turn((int)loc.getRelAngle(angle));
		// if(angle-loc.getDirection()>TURN_TOLLERANCE){
		// turn((int)loc.getRelAngle(angle));
		// }
		// if(angle-loc.getDirection()>TURN_TOLLERANCE){
		// turn((int)loc.getRelAngle(angle));
		// }

	}

}
