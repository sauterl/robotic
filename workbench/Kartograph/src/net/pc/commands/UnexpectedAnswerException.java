/**
 * 
 */
package net.pc.commands;

import net.packets.Packet;

/**
 * Exception to indicate that the answer was not expected.
 * @author loris.sauter
 *
 */
public class UnexpectedAnswerException extends Exception {
	
	private final static String defaultMessage = "The received packet is unexcepted";
	
	private final Packet packet;
	
	public Packet getPacket(){
		return packet;
	}

	/**
	 * 
	 */
	public UnexpectedAnswerException(Packet unexpected) {
		super(defaultMessage + ", packet id: "+(unexpected != null ? unexpected.getId() : "no packet found"));
		this.packet = unexpected;
	}

	/**
	 * @param message
	 */
	public UnexpectedAnswerException(String message, Packet unexpected) {
		super(message);
		this.packet = unexpected;
	}

}
