package pc;

/**
 * Interface to indicate a class is an executable task.
 * The TaskExecutor has an internal queue of pipes and executes each after another
 * 
 * @author loris.sauter
 *
 */
public interface Task {

	/**
	 * Executes the task.
	 * All relevant code goes into here.
	 * @param env The TaskExecutor which is the environment of this task
	 * @return TRUE on success, FALSE on failure.
	 */
	public boolean execute(TaskExecutor env);
}
