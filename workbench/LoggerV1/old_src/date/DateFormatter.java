package date;



public class DateFormatter {

	public static void main(String[] args) {
		int startY = 1970;
		long yearS = 31556926;//seconds (1y = ...s)
		long dayS = 86400;//seconds (1d = ...s)
		
		System.out.println(System.currentTimeMillis() / 1000);
		long unix = System.currentTimeMillis() / 1000;
		long year = unix / yearS;
		System.out.println(startY + year);
		long rest = unix - (year * yearS);
		System.out.println("rest: "+rest);
		long day = rest / dayS + 1;//timezone + 1?!
		System.out.println(day);
		DayInYear date = DayInYear.getDayInYear((int)day, (int)year);
		System.out.println(date.toString());
		
	}

}
