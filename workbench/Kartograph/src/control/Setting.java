package control;

import lejos.nxt.Motor;

public class Setting {
	/**
	 * Load motor settings for driving
	 */
	public static void driveSettings() {
		Motor.B.setSpeed(1500 * 2);
		Motor.C.setSpeed(1500 * 2);
		Motor.B.setAcceleration(1000 * 2);
		Motor.C.setAcceleration(1000 * 2);
	}

	// /**
	// * Load motor settings for driving with a given speed
	// *
	// * @param speed speed in degrees per second
	// */
	// public static void driveSettings(int speed) {
	// Motor.B.setSpeed(speed);
	// Motor.C.setSpeed(speed);
	// Motor.B.setAcceleration(1000);
	// Motor.C.setAcceleration(1000);
	// }

	/**
	 * Load motor settings for driving with a given speed and a given acceleration
	 * 
	 * @param speed speed in degrees per second
	 * @param acceleration acceleration in degrees accelerated per second
	 */
	public static void driveSettings(int speed, int acceleration) {
		Motor.B.setSpeed(speed);
		Motor.C.setSpeed(speed);
		Motor.B.setAcceleration(acceleration);
		Motor.C.setAcceleration(acceleration);
	}

	/**
	 * Settings for the measurement motor
	 * 
	 * @deprecated since the Measurement was separated from the robot control.
	 */
	@Deprecated
	public static void sensorMesureSetting() {
		Motor.A.setSpeed(360 * 2);
		Motor.A.setAcceleration(360 * 4);
	}

	/**
	 * Settings for the measurement motor for turning without measuring.
	 * 
	 * @deprecated since the Measurement was separated from the robot control.
	 */
	@Deprecated
	public static void sensorTurnSetting() {
		Motor.A.setSpeed(360 * 2);
		Motor.A.setAcceleration(720 * 4);
	}

	/**
	 * Load motor settings for turning
	 */
	public static void turnSettings() {
		Motor.B.setSpeed(1500);
		Motor.C.setSpeed(1500);
		Motor.B.setAcceleration(360);
		Motor.C.setAcceleration(360);
	}
}
