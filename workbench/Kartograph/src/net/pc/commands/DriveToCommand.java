/**
 * 
 */
package net.pc.commands;

import java.io.IOException;

import lejos.geom.Point;
import net.packets.EndOfStreamException;
import net.packets.NotSetupException;
import net.packets.Packet;
import net.packets.PacketException;
import net.packets.PointDataPacket;
import net.packets.commands.CmdDrive;
import net.packets.commands.OKCmdPacket;

/**
 * @author loris.sauter
 *
 */
@Deprecated
public class DriveToCommand extends AbstractCommand {
	
	private Point to, otherRobot, reached;

	/**
	 * @param pckt
	 */
	public DriveToCommand(Point to, Point otherRobot) {
		super(new CmdDrive() );
		this.to = to;
		this.otherRobot = otherRobot;
	}

	/** 
	 * {@inheritDoc}
	 * @see net.pc.commands.AbstractCommand#onReceive(net.packets.Packet)
	 */
	@Override
	public boolean onReceive(Packet answer) throws UnexpectedAnswerException {
		System.out.println("DriveToCommand, expecting reached point now");
		if(answer instanceof OKCmdPacket){
			//send the two points
			try{
				handler.send(new PointDataPacket(to));
				handler.send(new PointDataPacket(otherRobot));
			}catch(IOException | NotSetupException e){
				e.printStackTrace();
				return false;
			}
			Packet rec;
			try {
				rec = this.handler.getNextPacket();
				if(rec instanceof PointDataPacket){
					this.reached = ((PointDataPacket)rec).get();
					return true;
				}else{
					throw new UnexpectedAnswerException(rec);
				}
			} catch (PacketException e) {
				e.printStackTrace();
				return true;
			} catch (IOException e) {
				e.printStackTrace();
				return false;
			} catch (NotSetupException e) {
				e.printStackTrace();
				return false;
			} catch (EndOfStreamException e) {
				e.printStackTrace();
				return false;
			}
		}else{
			throw new UnexpectedAnswerException(answer);
		}
	}

}
