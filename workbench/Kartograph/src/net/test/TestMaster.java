package net.test;

import net.pc.BTChannelManager;
import net.pc.commands.ConnectCommand;
import net.pc.commands.RequestPacketTestCommand;

/**
 * Class to test the pc part of communication
 * @author loris.sauter
 *
 */
public class TestMaster {

	public static void main(String[] args) {
		System.out.println("Started TestMaster");
		BTChannelManager manager = new BTChannelManager();
		int[] ids = manager.getIDs();
		for(int id : ids){
			try {
				manager.putCommand(new ConnectCommand(), id);
				manager.putCommand(new RequestPacketTestCommand(), id );
//				for(int i=0; i<1000000; i++){
//					if(i%250000 == 0){
//						System.out.println("Waiting...(forced)");
//					}
//				}
//				manager.putCommand(new CloseCommand(), id);
			} catch (InterruptedException e) {
				System.out.println("Could not put new command");
				e.printStackTrace();
			}
		}

	}

}
