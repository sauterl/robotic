/**
 * 
 */
package net.nxt.commands;

import java.io.IOException;

import net.packets.MesureMapDataPacket;
import net.packets.NotSetupException;
import net.packets.Packet;
import net.packets.commands.CmdMesure;

/**
 * @author loris.sauter
 *
 */
public class MesureSequenz extends CommandSequenz {

	/**
	 * 
	 */
	public MesureSequenz() {
	}

	/** 
	 * {@inheritDoc}
	 * @see net.nxt.commands.CommandSequenz#executeSequenz(net.packets.Packet)
	 */
	@Override
	public boolean executeSequenz(Packet packet) throws SequenzException {
		if(packet instanceof CmdMesure){
			try {
				System.out.println("sending mmdp");
				boolean succ = interpreter.getController().mesure();
				System.out.println("Mesure: "+succ);
				handler.send(new MesureMapDataPacket(interpreter.getController().getMesMap()));
				return true;
			} catch (NotSetupException | IOException e) {
				System.out.println("ERR: send mmdp");
				e.printStackTrace();
			}
		}
		return false;
	}

}
