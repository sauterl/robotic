package mesure;

import lejos.geom.Point;
import lejos.nxt.Motor;
import lejos.nxt.SensorPort;
import lejos.nxt.UltrasonicSensor;
import vectormap.Chunk;
import vectormap.Line;
import control.Location;
import control.MesureMap;
import control.Setting;

public class Mesure {
	Chunk chunk;
	Location loc;
	UltrasonicSensor sens;
	int turnAngle = 0;

	public Mesure(Location loc, Chunk chunk) {
		this.loc = loc;
		this.chunk = chunk;
		Motor.A.resetTachoCount();
		sens = new UltrasonicSensor(SensorPort.S1);
		Setting.sensorMesureSetting();
	}

	// public void drawMinLine(MesureMap mesMap) {
	// float[] cord = null;
	// for (int i = 0; i < 5 && cord == null; i++) {
	// cord = takeMesure(mesMap);
	// }
	// if (cord == null) {
	// throw new UncorrespondingMesurementException();
	// }
	// float vecy = (float) (loc.getXC() - cord[0] * 1);
	// float vecx = (float) (loc.getYC() - cord[1] * -1 * 1);
	// float len = (float) Math.sqrt(Math.pow(vecy, 2) + Math.pow(vecx, 2));
	// vecy = vecy / len;
	// vecx = vecx / len;
	// chunk.addLine(new Line(cord[0] + vecx, cord[1] + vecx, cord[0] - vecx, cord[1] - vecx));
	// }

	public void forceUnreach(MesureMap m) {
		float dist = sens.getRange();
		// if (dist > 254) {
		if (dist > 150) {
			// return null;
		}
		dist = correct(dist);
		float[] coordinates = new float[2];
		coordinates[0] = (float) ((float) Math.cos(degToRad((loc.getDirection() + turnAngle)) % 360) * dist * 1 + loc
				.getXC());
		coordinates[1] = (float) ((float) Math.sin(degToRad((loc.getDirection() + turnAngle)) % 360) * dist * -1 + loc
				.getYC());
		// System.out.println(turnAngle + "\t" + coordinates[0] + "\t" + coordinates[1]);
		m.unreach(new Point(coordinates[0], coordinates[1]));
		// return coordinates;
	}

	public float mesureFront() {
		toZero();
		float dist = sens.getRange();
		return dist;
	}

	public void MesureWithLog360() throws MesureException {
		if (Motor.B.isMoving() || Motor.C.isMoving()) {
			throw new MesureException(
					"Wasn't able to make an accurate mesure because the Robot is Moving! please try again when the Robot is standing.");
		}
		toZero();
		int stepSize = 10;
		float[] prev = null;
		float[] act = null;
		for (int i = 0; i < 360; i += stepSize) {
			if (act != null) {
				prev = act;
			}
			sensorTurn(stepSize, false);
			float[] tmp = takeMesure();
			if (tmp != null) {
				act = tmp;
			}

			if (prev != null && act != null) {
				lineCheck(prev, act);
			}

		}
		toZero(true);
	}

	public void MesureWithLog360(MesureMap mesMap) throws MesureException {

		if (Motor.B.isMoving() || Motor.C.isMoving()) {
			throw new MesureException(
					"Wasn't able to make an accurate mesure because the Robot is Moving! please try again when the Robot is standing.");
		}

		toZero();
		int stepSize = 10;
		float[] prev = null;
		float[] act = null;
		for (int i = 0; i < 360; i += stepSize) {
			if (act != null) {
				prev = act;
			}
			sensorTurn(stepSize, false);
			float[] tmp = takeMesure(mesMap);
			if (tmp != null) {
				act = tmp;
			}

			if (prev != null && act != null) {
				lineCheck(prev, act);
			}

		}
		System.out.println("0: " + loc.getPosition());
		mesMap.mesured(loc.getPosition());
		System.out.println("1: " + loc.getPositionC());
		mesMap.mesured(loc.getPositionC());

		toZero(false);
	}

	public float[] takeMesure(MesureMap m) {
		float dist = sens.getRange();
		// if (dist > 254) {
		if (dist > 150) {
			return null;
		}
		dist = correct(dist);
		float[] coordinates = new float[2];
		coordinates[0] = (float) ((float) Math.cos(degToRad((loc.getDirection() + turnAngle)) % 360) * dist * 1 + loc
				.getXC());
		coordinates[1] = (float) ((float) Math.sin(degToRad((loc.getDirection() + turnAngle)) % 360) * dist * -1 + loc
				.getYC());
		// System.out.println(turnAngle + "\t" + coordinates[0] + "\t" + coordinates[1]);
		m.unreach(new Point(coordinates[0], coordinates[1]));
		return coordinates;
	}

	private float correct(float dist) {
		if (dist < 23) {
			dist -= 3;
		} else if (dist < 25) {
			dist -= 2;
		} else if (dist < 27) {
			dist -= 1;
		} else if (dist < 29) {

		} else if (dist < 58) {
			dist += 1;
		} else if (dist < 84) {
			dist += 2;
		} else if (dist < 90) {
			dist += 3;
		} else if (dist < 104) {
			dist += 2;
		} else if (dist < 126) {
			dist += 3;
		} else if (dist < 128) {
			dist += 2;
		}
		return dist;
	}

	private double degToRad(double deg) {
		return (deg / 180) * Math.PI;
	}

	private void lineCheck(float[] p1, float[] p2) {
		float maxLenght = 50;
		if (Math.sqrt(Math.pow(p1[0] - p2[0], 2) + Math.pow(p1[1] - p2[1], 2)) < maxLenght) {
			chunk.addLine(new Line(p1[0], p1[1], p2[0], p2[1]));
		}
	}

	private void sensorTurn(int angle, boolean iRet) {
		Motor.A.rotate(angle, iRet);
		turnAngle += angle;
	}

	private float[] takeMesure() {
		float dist = sens.getRange();
		// if (dist > 254) {
		if (dist > 100) {
			return null;
		}
		dist = correct(dist);
		float[] coordinates = new float[2];
		coordinates[0] = (float) ((float) Math.cos(degToRad((loc.getDirection() + turnAngle)) % 360) * dist * 1 + loc
				.getXC());
		coordinates[1] = (float) ((float) Math.sin(degToRad((loc.getDirection() + turnAngle)) % 360) * dist * -1 + loc
				.getYC());
		// System.out.println(turnAngle + "\t" + coordinates[0] + "\t" + coordinates[1]);
		return coordinates;
	}

	private void toZero() {
		if (turnAngle != 0) {
			Setting.sensorTurnSetting();
			sensorTurn(-1 * turnAngle, false);
			Setting.sensorMesureSetting();
		}
	}

	private void toZero(boolean iRet) {
		if (turnAngle != 0) {
			Setting.sensorTurnSetting();
			sensorTurn(-1 * turnAngle, iRet);
			Setting.sensorMesureSetting();
		}
	}

}
