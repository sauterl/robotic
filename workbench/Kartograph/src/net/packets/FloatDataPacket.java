package net.packets;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class FloatDataPacket implements DataPacket<Float> {
	
	private float f;
	
	public FloatDataPacket(){}
	
	public FloatDataPacket(float f){
		this.f = f;
	}

	@Override
	public int getId() {
		return PacketIDRegistry.FLOAT;
	}

	@Override
	public Float get() {
		return f;
	}

	@Override
	public void set(Float e) {
		this.f = e;
	}

	@Override
	public void write(DataOutputStream to) throws IOException {
		to.write(this.getId());
		to.writeFloat(f);
		to.flush();
	}

	@Override
	public void read(DataInputStream from) throws IOException {
		f = from.readFloat();
	}

}
