/**
 * 
 */
package net.nxt.commands;

import java.io.IOException;

import net.packets.NotSetupException;
import net.packets.Packet;
import net.packets.commands.OKCmdPacket;

/**
 * @author loris.sauter
 *
 */
public class ConnectSequenz extends CommandSequenz {


	/** 
	 * {@inheritDoc}
	 * @see net.nxt.commands.CommandSequenz#executeSequenz()
	 */
	@Override
	public boolean executeSequenz(Packet packet) throws SequenzException {
		try {
			handler.send(new OKCmdPacket());
			return true;
		} catch (IOException e) {
			System.out.println("ERR: IO");
			e.printStackTrace();
			return false;
		} catch (NotSetupException e) {
			System.out.println("ERR: PH setup");
			e.printStackTrace();
			return false;
		}
	}

}
