package net.packets.commands;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import net.packets.PacketIDRegistry;

public class ReqTestCmdPacket implements CommandPacket {

	public ReqTestCmdPacket() {
	}

	@Override
	public int getId() {
		return PacketIDRegistry.CMD_TEST;
	}

	@Override
	public void write(DataOutputStream to) throws IOException {
		to.write(this.getId() );
		to.flush();
	}

	@Override
	public void read(DataInputStream from) throws IOException {
		
	}

	@Override
	public void executeCommand() {

	}

}
