package net.packets;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 * Class to provide the sending of strings over DataInput and DataOutputstreams.
 * @author Loris
 *
 */
public class StringDataPacket implements DataPacket<String> {

	private String string;
	
	public StringDataPacket(){}
	
	
	public StringDataPacket(String str){
		this.string = str;
	}

	
	@Override
	public int getId() {
		return PacketIDRegistry.STRING;
	}

	@Override
	public void write(DataOutputStream to) throws IOException {
		to.write(this.getId());
		to.writeUTF(this.string);
		to.flush();
	}

	@Override
	public void read(DataInputStream from) throws IOException {
		this.string = from.readUTF();

	}


	@Override
	public String get() {
		return this.string;
	}


	@Override
	public void set(String e) {
		this.string = e;
	}

}
