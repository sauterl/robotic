/**
 * 
 */
package net.packets.commands;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import lejos.geom.Point;
import net.packets.PacketIDRegistry;

/**
 * @author loris.sauter
 *
 */
public class CmdNextPoint implements CommandPacket {
	
	private Point otherRobot;
	
	public CmdNextPoint(){
		
	}
	
	public CmdNextPoint(Point otherRobot){
		this.otherRobot = otherRobot;
	}

	@Override
	public int getId() {
		return PacketIDRegistry.CMD_NEXTPOINT;
	}

	@Override
	public void write(DataOutputStream to) throws IOException {
		to.write(this.getId() );
		to.writeFloat(otherRobot.x);
		to.writeFloat(otherRobot.y);
		to.flush();
	}

	@Override
	public void read(DataInputStream from) throws IOException {
		this.otherRobot = new Point(0,0);
		otherRobot.x = from.readFloat();
		otherRobot.y = from.readFloat();
	}
	
	public Point getOtherRobotPosition(){
		return otherRobot;
	}

	@Override
	public void executeCommand() {
		
	}

}
