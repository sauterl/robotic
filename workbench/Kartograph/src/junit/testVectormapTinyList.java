package junit;

import static org.junit.Assert.*;

import org.junit.Test;

import vectormap.OversizeException;
import vectormap.TinyList;

public class testVectormapTinyList {

	@Test
	public void testConstructor() {
		TinyList<Integer> li=new TinyList<Integer>((byte) 4);
		if(li.size()!=0){
			fail("constructed empty list with a size != 0");
		}
	}
	@Test
	public void testAdd(){
		TinyList<Integer> li=new TinyList<Integer>((byte) 4);
		for(int i=0;i<1000;i++){
			li.add(i);
			if(li.size()!=i+1){
				fail("extending is handled wrong");
			}
		}
		try{
		for(int i=0;i<Integer.MAX_VALUE;i++){
			li.add(i);
		}
		fail("ecxeption wasnt thrown");
		}catch(OversizeException e){
			
		}
		for(int i=0;i<Short.MAX_VALUE-4;i++){
			try{
			int a = li.getEntry((short)i);
			}catch(IndexOutOfBoundsException e){
				fail("couldent reach a Index initialized before. index was "+i);
			}
		}
		try{
			li.getEntry((short)-5);
			fail("exepted a negative index");
		}catch (IndexOutOfBoundsException e){
			
		}
		while(li.size()>0){
			li.remove(0);
		}
		try{
			li.getEntry((short) 0);
			fail("dident throw exception it the indes is out of bounds or didnt remove the last element of the TinyList");
		}catch(IndexOutOfBoundsException e){
			
		}
	}

}
