package vectormap;

import java.util.ArrayList;

/**
 * This is a list similar to an {@link ArrayList} but uses less resources. This is because a {@code TinyList} wont
 * internal resize to a 150 Percent of its size but will only add the specified amount of spaces. In addition to this
 * the indexes can only be a short value. This class is used for lists that shouldent get huge and should save RAm. be
 * aware that a {@code TinyList}wont save any computing power it will usually use more.
 * 
 * @author Eddie
 *
 * @param <E> the class of the list entrys
 */
public class TinyList<E> {
	Object[] list;
	/**
	 * spaces that will be added if the {@code TinyList} needs to be extended.
	 */
	private byte resizeadd;
	/**
	 * Size of the {@code TinyList}
	 */
	private short size;

	/**
	 * Constructor for a {@code TinyList}
	 * 
	 * @param resizeadd spaces that will be added if the {@code TinyList} needs to be extended.
	 */
	public TinyList(byte resizeadd) {
		list = new Object[resizeadd];
		// size = resizeadd;
		this.resizeadd = resizeadd;
	}

	/**
	 * Add e entry to the {@code TinyList}
	 * 
	 * @param entry entry to be added.
	 */
	public void add(E entry) {
		if (size < list.length) {
			list[size] = entry;
			size++;
		} else {
			extend();
			add(entry);
		}
	}

	/**
	 * Get a specific entry of the list
	 * 
	 * @param indexindex of the entry
	 * @return the entry with the given index
	 */
	@SuppressWarnings("unchecked")
	public E getEntry(short index) {
		if (index < size || index < 0) {
			return (E) list[index];
		} else {
			throw new IndexOutOfBoundsException("The given index doesn't correspond with a entry");
		}
	}

	/**
	 * removes a specific entry from the List
	 * 
	 * @param index index of the entry to remove
	 */
	public void remove(int index) {
		for (int i = 0; i < size; i++) {
			if (i > index) {
				list[i - 1] = list[i];
			}
		}
		size--;
		if (list.length - size > resizeadd) {
			shrink();
		}
	}

	/**
	 * get the size of the {@code TinyList}
	 * 
	 * @return
	 */
	public int size() {
		return size;
	}

	/**
	 * extends the {@code TinyList} to offer more space for a new entry
	 */
	private void extend() {
		Object[] tmp = new Object[list.length + resizeadd];
		for (int i = 0; i < size; i++) {
			tmp[i] = list[i];
		}
		if (resizeadd < (Short.MAX_VALUE - size)) {
			list = tmp;
		} else {
			throw new OversizeException(
					"The Tinylist has run out of larger indexes. the maximum entrys in a tinylist are "
							+ (32767 - resizeadd)
							+ "If more space is needed it is recomented to either use an Arraylist ore use a smaller resizeadd.");
		}
	}

	/**
	 * shrinks the {@code TinyList} in order to save space.
	 */
	private void shrink() {
		if (size != 0) {
			Object[] tmp = new Object[list.length - resizeadd];
			for (int i = 0; i < size; i++) {
				tmp[i] = list[i];
			}
			list = tmp;
		}
	}
}
