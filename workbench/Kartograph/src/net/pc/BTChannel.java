package net.pc;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.util.concurrent.ArrayBlockingQueue;

import lejos.pc.comm.NXTComm;
import lejos.pc.comm.NXTCommException;
import lejos.pc.comm.NXTInfo;
import logic.PCControler;
import net.packets.BasePacketHandler;
import net.packets.EndOfStreamException;
import net.packets.NotSetupException;
import net.packets.Packet;
import net.packets.PacketException;
import net.packets.PacketIDRegistry;
import net.pc.commands.AbstractCommand;
import net.pc.commands.CloseCommand;
import net.pc.commands.ClosingConnectionException;
import net.pc.commands.UnexpectedAnswerException;

/**
 * The BTChannel is a communication channel between a serving pc thread and a
 * nxt brick. The communication is divided into several commands
 * (AbstractCommand) whose receiving cause the nxt to do something.
 * 
 * The command must have an underlying CommandPacket, which is supported by the
 * bricks CommandInterpreter.
 * 
 * @author loris.sauter
 * 
 */
public class BTChannel implements Runnable {

	private volatile boolean running = true;

	private DataInputStream dis;
	private DataOutputStream dos;
	private NXTComm comm;
	private NXTInfo nfo;
	private BasePacketHandler ph;

	private PCControler ctrl;
	
	private ArrayBlockingQueue<AbstractCommand> cmds;
	private static final int cmdsCap = 5;// test size!

	public NXTInfo getInfo() {
		return nfo;
	}

	public BTChannel(NXTComm communication, NXTInfo info, PCControler ctrl)
			throws NXTCommException {
		this.ctrl = ctrl;
		cmds = new ArrayBlockingQueue<AbstractCommand>(cmdsCap);
		comm = communication;
		nfo = info;
		System.out.println("BTC early init done");
		comm.open(info);
		System.out.println("BTC opened comm");
		dis = new DataInputStream(comm.getInputStream());
		dos = new DataOutputStream(comm.getOutputStream());
		ph = new BasePacketHandler();
		ph.setup(dis, dos);
		System.out.println("BTC init done");
	}

	/**
	 * Returns the BasePacketHandler which handles the packets on this
	 * BTChannel.
	 * 
	 * @return the BasePacketHandler which handles the packets on this
	 *         BTChannel.
	 */
	public BasePacketHandler getPacketHandler() {
		return ph;
	}

	/**
	 * Adds the given command to the queue of commands to send.
	 * 
	 * @param cmd
	 *            The command to send
	 * @throws InterruptedException
	 *             If the command queue is full and it cannot wait for space.
	 * @see ArrayBlockingQueue#put(Object)
	 */
	public void addCommand(AbstractCommand cmd) throws InterruptedException {
		cmds.put(cmd);
	}

	/**
	 * Effectively runs the channel: Sends one command after each other and
	 * blocks if no more commands are available. It does call the commands
	 * onReceive if an answer got received.
	 * 
	 * Original doc: {@inheritDoc}
	 * 
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		System.out.println("BTChannel startet running");
		while (running) {
			AbstractCommand currCmd = null;
			try {
				currCmd = cmds.take();
				System.out.println("found cmd");
			} catch (InterruptedException e) {
				System.err.println("Cannot wait till next command!");
				e.printStackTrace();
				break;
			}
			if (currCmd != null) {
				Packet toSend = currCmd.getCmdPacket();
				if (toSend != null) {
					try {
						ph.send(toSend);
						System.out.println("sent cmdpckt");
					} catch (IOException e) {
						System.err
								.println("Error while writing command packet. Terminating");
						e.printStackTrace();
						break;
					} catch (NotSetupException e) {
						System.err.println("PacketHandler not set up");
						e.printStackTrace();
						break;
					}
					Packet answer = null;
					try {
						System.out.println("Waiting for answer");
						answer = ph.getNextPacket();
					} catch (EOFException e) {
						System.out.println("Proper closing");
						break;
					} catch (PacketException e) {
						System.err.println("Packetexception");
						e.printStackTrace();
					} catch (IOException e) {
						System.err.println("io error");
						e.printStackTrace();
						break;
					} catch (NotSetupException e) {
						System.err.println("packethandler not setup");
						e.printStackTrace();
						break;
					} catch (EndOfStreamException e) {
						System.out.println("EoS reached");
						break;
					}
					if (answer != null) {
						System.out
								.println("Received answer: " + answer.getId());
						if (answer.getId() == toSend.getId()) {
							performRequestedClosing();
						} else if (answer.getId() == PacketIDRegistry.CMD_CLOSE) {
							try {
								currCmd.onClose();
							} catch (ClosingConnectionException e) {
								performRequestedClosing();
							}
						} else {
							currCmd.setPacketHandler(ph);
							currCmd.setController(ctrl);
							try {
								currCmd.onReceive(answer);
							} catch (UnexpectedAnswerException e) {
								if (e.getPacket().getId() == PacketIDRegistry.CMD_CLOSE) {
									System.out
											.println("Unexpected close. Closing now");
									try {
										currCmd.onClose();
									} catch (ClosingConnectionException ex) {
										performRequestedClosing();
									}

								} else {
									System.out
											.println("Unexpected packet, what to do?");
								}
							}
							currCmd.removePacketHandler();
							currCmd.removeController();
						}

					}
				}
			}
		}
		performClosing();
	}

	/**
	 * Performs a requested close. A requested close means it returns close to
	 * the nxt brick and then closes all linked resource
	 */
	private void performRequestedClosing() {
		System.out.println("performing requested close");
		this.running = false;
		AbstractCommand cmd = new CloseCommand();
		Packet p = cmd.getCmdPacket();
		try {
			p.write(dos);
			System.out.println("Wrote close cmd");
		} catch (IOException e) {
			System.out.println("Could not write close commad");
			e.printStackTrace();
		}
		performClosing();
	}

	/**
	 * Performs the internal closing of all linked resources
	 */
	private void performClosing() {
		System.out.println("Performing close...");
		try {
			dis.close();
			System.out.println("Closed DIS");
		} catch (IOException ex) {
			System.err.println("Exception on closing DataInputStream");
			ex.printStackTrace();
		}
		try {
			dos.close();
			System.out.println("Closed DOS");
		} catch (IOException ex) {
			System.err.println("Exception on closing DataOutputStream");
			ex.printStackTrace();
		}
		try {
			comm.close();
			System.out.println("Closed COMM");
		} catch (IOException ex) {
			System.err.println("Exception on closing NXTComm");
			ex.printStackTrace();
		}
	}

}
