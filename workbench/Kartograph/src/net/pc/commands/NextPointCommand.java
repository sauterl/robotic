/**
 * 
 */
package net.pc.commands;

import java.io.IOException;

import lejos.geom.Point;
import net.packets.EndOfStreamException;
import net.packets.MesureMapDataPacket;
import net.packets.NotSetupException;
import net.packets.Packet;
import net.packets.PacketException;
import net.packets.PointDataPacket;
import net.packets.commands.CmdDrive;
import net.packets.commands.CmdNextPoint;
import net.packets.commands.OKCmdPacket;
import control.MesureMap;

/**
 * @author loris.sauter
 *
 */
public class NextPointCommand extends AbstractCommand{
	
	private Point otherRobot;
	private Point reached;
	private Point toGo;
	private MesureMap map;

	/**
	 * 
	 */
	public NextPointCommand(Point otherRobot, MesureMap map) {
		super(new CmdNextPoint(otherRobot));
		this.otherRobot = otherRobot;
		this.map = map;
	}
	
	public Point getReached(){
		return reached;
	}

	@Override
	public boolean onReceive(Packet answer) throws UnexpectedAnswerException {
		Packet out = answer;
		if(answer instanceof OKCmdPacket){
			//send map
			try {
				handler.send( new MesureMapDataPacket(map) );
			} catch (NotSetupException | IOException e) {
				e.printStackTrace();
				return false;
			}
		}
		try {
			answer = handler.getNextPacket();
		} catch (PacketException | IOException | NotSetupException
				| EndOfStreamException e1) {
			e1.printStackTrace();
			return false;
		}
		if(answer instanceof PointDataPacket){
			toGo = ((PointDataPacket) answer).get();
			// TODO DO STUFF WITH LOGIC LOC DRIVING NOW
			try {
				handler.send(new CmdDrive(toGo, otherRobot));
				try {
					Packet rec = handler.getNextPacket();
					if(rec instanceof PointDataPacket){
						handler.readPacket(rec);
						reached = ((PointDataPacket) rec).get();
						//UNLOCK DRIVING
						return true;
					}
				} catch (PacketException | EndOfStreamException e) {
					e.printStackTrace();
					return false;
				}
				
			} catch (NotSetupException | IOException e) {
				e.printStackTrace();
				return false;
			}
		}
		throw new UnexpectedAnswerException(out);
	}

}
