import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.io.Writer;

import lejos.nxt.*;
import lejos.util.Delay;
public class Exam {
	private static final double DIAMETER=5.6;
	private static double distance_done=0;
	public static void main(String []args) {
		UltrasonicSensor son=new UltrasonicSensor(SensorPort.S1);
		Delay a=new Delay();
		System.out.println("test");
		System.out.println("test2");
		LogMesur mes=new LogMesur();
		Motor.A.setSpeed(360);
		
		
		for(int asda=0;asda<5;asda++){
		Mapper map=new Mapper();
		Motor.A.resetTachoCount();
		//LCD.clear();
		
		for(int i=0;i<90;i++){
			Motor.A.rotate(-4);
//			LCD.clear();
			int distance=0;
//			int [] dist=new int[10];
			int count=0;
			for(int x=0;x<5;x++){
				int t=son.getDistance();
				if(t==255){
					
				}else{
					distance+=t;
					count++;
				}
			}
			if (count==0){
				distance=255;
			}else{
				distance=distance/count;
			}
			
			int angle=i*4;
			mes.makeLog(distance,angle,distance_done);
			map.mapPoint(distance, angle);
		}
		
		Motor.A.rotate(360);
//		Button.waitForAnyPress();
		
		
		Motor.B.setSpeed(1000);
		Motor.C.setSpeed(1000);
		Motor.B.setAcceleration(400);
		Motor.C.setAcceleration(400);
		Motor.B.rotate(-360*3,true);
		Motor.C.rotate(-360*3);
//		a.msDelay(1000);
//		Motor.C.stop(true);
//		Motor.B.stop();
		distance_done+= Math.PI*DIAMETER*3;
		}
		mes.closeFile();
	}
}

class Mapper{
	
	public Mapper(){
		LCD.clear();
		LCD.setPixel(50, 32, 1);
		LCD.setPixel(50, 31, 1);
		LCD.setPixel(49, 32, 1);
		LCD.setPixel(49, 31, 1);
	}
	
	public void mapPoint(double distance, double angle){
		double x,y;
		angle=(angle/180)*Math.PI;
		x=Math.sin(angle)*distance;
		y=Math.cos(angle)*distance;
		double sx=Math.sin(angle)*1;
		double sy=Math.cos(angle)*1;
		while(Math.abs(x)<255&&Math.abs(y)<255){
		setPixel(x,y);
		x+=sx;
		y+=sy;
		}
	}
	private void setPixel(double x,double y){
		x=x/255*50;
		y=y/255*-32;
		LCD.setPixel(50+(int)x, 32+(int)y, 1);
	}
}

class LogMesur{
	DataOutputStream writer;
	public LogMesur(){
		
		FileOutputStream out=null;
		File data=new File("log.txt");
		try{
		out=new FileOutputStream(data);
		}catch (FileNotFoundException e){
			System.out.println("problem");
		}
		writer=new DataOutputStream(out);
		try {
			writer.writeBytes("angle deg\tangle rad\tx\ty\r");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void makeLog(double distance, double angle,double distd){
		if(distance>254){
			return;
		}
		
		double x,y;
		String a ="";
		a+=angle;
		a+="\t";
		angle=(angle/180)*Math.PI;
		x=Math.sin(angle)*distance;
		y=Math.cos(angle)*distance+distd;;
//		String a ="";
		a+=Double.toString(angle);
		a+="\t";
		a+=Double.toString(x);
		a+="\t";
		a+=Double.toString(y);
		a+="\r";
		 try {
			writer.writeBytes(a);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void closeFile(){
		try {
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
