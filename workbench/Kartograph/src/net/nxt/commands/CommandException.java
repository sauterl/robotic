package net.nxt.commands;

public class CommandException extends Exception {

	public CommandException() {
		super("ERR: cmd");
	}

	public CommandException(String message) {
		super(message);
	}


}
