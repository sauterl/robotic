package vectormap;

public class DefaultIndexer implements Indexabel<Object> {
	/**
	 * This is the default index generator. this method will always return the value 1;
	 */
	@Override
	public short getindex(Object element) {
		return 0;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public short getNumIndex() {
		return 1;
	}

}
