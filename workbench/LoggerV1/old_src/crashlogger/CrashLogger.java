/**
 * 
 */
package crashlogger;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.lang.Thread.UncaughtExceptionHandler;

import lejos.nxt.Button;
import lejos.nxt.VM;

/**
 * A logger that logs the crash of a thread.
 * It implements the {@link UncaughtExceptionHandler} interface for that purpose.
 * 
 * There are in future two ways to log the crash:
 * One by simply displaying the last exception,
 * the other by creating a decent crash report file.
 * 
 * @author loris.sauter
 * @version 0.1
 *
 */
public class CrashLogger implements UncaughtExceptionHandler {
	
	private Configuration config;
	private ByteArrayOutputStream baos = null;
	private PrintStream ps;
	
	public CrashLogger(){
		//this.config;
		baos = new ByteArrayOutputStream();
		ps = new PrintStream(baos);
	}
	
	public CrashLogger(Configuration config){
		this.config = config;
		baos = new ByteArrayOutputStream();
		ps = new PrintStream(baos);
	}
	

	/**
	 * 
	 * @see java.lang.Thread.UncaughtExceptionHandler#uncaughtException(java.lang.Thread, java.lang.Throwable)
	 */
	@Override
	public void uncaughtException(Thread thread, Throwable cause) {
		String msg = cause != null ? getClassName(cause.getClass()) : "N/A";
		String threadName = thread != null ? thread.getName(): "N/A";
		String info = (msg != null ? msg : "null")  + "\nIn thread:\n" + (threadName != null ? threadName : "null");
		
		cause.printStackTrace(ps);
		
		
		System.out.println(info);
		System.out.println("");
		//ps.flush();
		
		System.out.println(baos.toString());
		
		Button.waitForAnyPress();
		System.exit(-1);
	}
	
	private String getClassName(Class clazz){
		String str = "N/A";
		VM.VMClass vmClass = VM.getVM().getVMClass(clazz);
		str = vmClass.getJavaClass().toString();
		return null;
	}

}
