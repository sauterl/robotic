import lejos.nxt.Motor;


public class Setting {
	public static void turnSettings() {
		Motor.B.setSpeed(1500);
		Motor.B.setSpeed(1500);
		Motor.B.setAcceleration(360);
		Motor.B.setAcceleration(360);
	}
	
	public static void driveSettings(){
		Motor.B.setSpeed(1500);
		Motor.B.setSpeed(1500);
		Motor.B.setAcceleration(1000);
		Motor.B.setAcceleration(1000);
	}
	
	public static void driveSettings(int speed){
		Motor.B.setSpeed(speed);
		Motor.B.setSpeed(speed);
		Motor.B.setAcceleration(1000);
		Motor.B.setAcceleration(1000);
	}
	
	public static void driveSettings(int speed,int acceleration){
		Motor.B.setSpeed(speed);
		Motor.B.setSpeed(speed);
		Motor.B.setAcceleration(acceleration);
		Motor.B.setAcceleration(acceleration);
	}
	
	public static void sensorMesureSetting(){
		Motor.A.setSpeed(360);
		Motor.A.setAcceleration(360);
	}
	
	public static void sensorTurnSetting(){
		Motor.A.setSpeed(360);
		Motor.A.setAcceleration(720);
	}
}
