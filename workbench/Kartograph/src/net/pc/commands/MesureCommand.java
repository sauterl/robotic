/**
 * 
 */
package net.pc.commands;

import java.io.IOException;

import net.packets.EndOfStreamException;
import net.packets.MesureMapDataPacket;
import net.packets.NotSetupException;
import net.packets.Packet;
import net.packets.PacketException;
import net.packets.commands.CmdMesure;
import net.packets.commands.OKCmdPacket;
import control.MesureMap;

/**
 * @author loris.sauter
 *
 */
public class MesureCommand extends AbstractCommand {
	
	private MesureMap rec;

	/**
	 * @param pckt
	 */
	public MesureCommand() {
		super(new CmdMesure());
	}
	
	public MesureMap getReceivedMap(){
		return rec;
	}

	/** 
	 * {@inheritDoc}
	 * @see net.pc.commands.AbstractCommand#onReceive(net.packets.Packet)
	 */
	@Override
	public boolean onReceive(Packet answer) throws UnexpectedAnswerException {
		if(answer instanceof MesureMapDataPacket){
			this.rec = ((MesureMapDataPacket)answer).get();
			//MESURE UNLOCK (lock before this command! may override the getCmdPacket
			return true;
		}
		throw new UnexpectedAnswerException(answer);
		
	}

}
